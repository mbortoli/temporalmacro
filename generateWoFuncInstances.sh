#!/bin/bash
FILES="./instances/*"
for f in $FILES
do
  echo "Processing $f file..."
  # take action on each file. $f store current file name
  basename "$f"
  g="$(basename -- $f)"
  #echo $f
  #echo "$g"
  python deleteFunctions.py $f "./wofuncinstances/$g"
  #cat "$f"
done
