
(define (domain satellite)
  (:requirements :strips :equality :typing :durative-actions)
(:types satellite direction instrument mode)
 (:predicates 
(posaux_pointing  ?s - satellite ?d - direction)
(negaux_pointing  ?s - satellite ?d - direction)
               (on_board ?i - instrument ?s - satellite)
	       (supports ?i - instrument ?m - mode)
	       (pointing ?s - satellite ?d - direction)
	       (power_avail ?s - satellite)
	       (power_on ?i - instrument)
	       (calibrated ?i - instrument)
	       (have_image ?d - direction ?m - mode)
	       (calibration_target ?i - instrument ?d - direction))
 
 


(:durative-action turn_to_calibrate
   :parameters ( ?s - SATELLITE  ?d - DIRECTION  ?d_prev - DIRECTION  ?i - INSTRUMENT )
   :duration (= ?duration 10 ) 
   :condition (and 
   (at start (pointing ?s ?d_prev))
   (over all (calibration_target ?i ?d))
   (over all (on_board ?i ?s))
   (over all (power_on ?i))
   (at end (power_on ?i))
   (over all (not (= ?d ?d_prev)))
   (at start (negaux_pointing ?s ?d))
   (at start (posaux_pointing ?s ?d))
   )
   :effect (and 
   (at start (not (pointing ?s ?d_prev)))
   (at start (not (negaux_pointing ?s ?d)))
   (at end (pointing ?s ?d))
   (at end (calibrated ?i))
   (at end (negaux_pointing ?s ?d))
   )
)
  (:durative-action turn_to_take_image
   :parameters ( ?s - SATELLITE  ?d - DIRECTION  ?d_prev - DIRECTION  ?i - INSTRUMENT  ?m - MODE )
   :duration (= ?duration 12 ) 
   :condition (and 
   (at start (pointing ?s ?d_prev))
   (over all (calibrated ?i))
   (over all (supports ?i ?m))
   (over all (on_board ?i ?s))
   (over all (power_on ?i))
   (over all (not (= ?d ?d_prev)))
   (at end (power_on ?i))
   (at start (negaux_pointing ?s ?d))
   (at start (posaux_pointing ?s ?d))
   )
   :effect (and 
   (at start (not (pointing ?s ?d_prev)))
   (at start (not (negaux_pointing ?s ?d)))
   (at end (pointing ?s ?d))
   (at end (have_image ?d ?m))
   (at end (negaux_pointing ?s ?d))
   )
)
  (:durative-action switch_on
   :parameters ( ?i - INSTRUMENT  ?s - SATELLITE )
   :duration (= ?duration 2 ) 
   :condition (and 
   (at start (power_avail ?s))
   (over all (on_board ?i ?s))
   )
   :effect (and 
   (at start (not (calibrated ?i)))
   (at start (not (power_avail ?s)))
   (at end (power_on ?i))
   )
)
  (:durative-action switch_off
   :parameters ( ?i - INSTRUMENT  ?s - SATELLITE )
   :duration (= ?duration 1 ) 
   :condition (and 
   (at start (power_on ?i))
   (over all (on_board ?i ?s))
   )
   :effect (and 
   (at start (not (power_on ?i)))
   (at end (power_avail ?s))
   )
)

)

