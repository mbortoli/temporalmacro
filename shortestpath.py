#!/usr/bin/python

import sys, getopt


   
def main(argv):
   inputfile = argv[0]
   reading = open(inputfile,'r')
   lines = reading.readlines()
   tlocation = []
   dlocation = []
   check = False
   for line in lines:
      #print line
      if check and ")" in line and not ("(" in line and ")" in line):
         check = False 
      elif check and str(line).strip()[0].strip() == "s" and line.split(" - ")[1].strip() == "location":
         tlocation.append(line.split(" - ")[0].strip())
         dlocation.append(line.split(" - ")[0].strip())
      elif check and str(line).strip()[0].strip() == "p" and line.split(" - ")[1].strip() == "location":
         dlocation.append(line.split(" - ")[0].strip())      
      if "(:objects" in line:
            check = True
   print tlocation
   print dlocation
   
   for l1 in tlocation:
      for l2 in tlocation:
         if l1 != l2:
            print "(link " + l1 + " " + l2 + ")"
         
   for l1 in dlocation:
      for l2 in dlocation:
         if l1 != l2:
            print "(path " + l1 + " " + l2 + ")"
   
   
if __name__ == "__main__":
   main(sys.argv[1:])
