import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.sass']
})
export class OrdersComponent implements OnInit {

  @Input()
  orders: any[];

  @Input()
  rings: any[];

  constructor() { }

  ngOnInit() {
    console.log(this.orders);
  }

}
