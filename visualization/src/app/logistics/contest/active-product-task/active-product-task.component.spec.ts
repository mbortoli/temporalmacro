import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActiveProductTaskComponent } from './active-product-task.component';

describe('ActiveProductTaskComponent', () => {
  let component: ActiveProductTaskComponent;
  let fixture: ComponentFixture<ActiveProductTaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActiveProductTaskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActiveProductTaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
