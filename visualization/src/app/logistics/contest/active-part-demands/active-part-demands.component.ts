import {AfterViewInit, Component, Input, OnInit, ViewChild} from '@angular/core';
import {NestedTreeControl} from "@angular/cdk/tree";
import {MatTreeNestedDataSource} from "@angular/material/tree";

@Component({
  selector: 'app-active-part-demands',
  templateUrl: './active-part-demands.component.html',
  styleUrls: ['./active-part-demands.component.sass']
})
export class ActivePartDemandsComponent implements OnInit {

  @Input() data;

  treeControl = new NestedTreeControl<any>(node => node.children);
  dataSource = new MatTreeNestedDataSource<any>();

  constructor() { }

  ngOnInit() {
  }

  updateData() {
    if (this.data == undefined) {
      return;
    }
    let tmp = this.data.reduce((obj, ele) => {
      if (obj.filter(x => x.machine == ele.machine)[0] == undefined) {
        obj.push({
          machine: ele.machine,
          children: []
        });
      }
      obj.filter(x => x.machine == ele.machine)[0].children.push(ele);
      return obj;
    }, []);
    this.dataSource.data = tmp;
    this.treeControl.dataNodes = tmp;
    this.treeControl.dataNodes.forEach(node => this.treeControl.expand(node));
  }

  hasChild = (_: number, node: any) => !!node.children && node.children.length > 0;

}
