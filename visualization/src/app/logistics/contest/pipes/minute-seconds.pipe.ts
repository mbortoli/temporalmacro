import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'minuteSeconds'
})
export class MinuteSecondsPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    value = Math.floor(value);
    let minutes: any = Math.floor(value / 60);
    let seconds: any = (value - minutes * 60);
    if (seconds < 10) {
      seconds = "0" + seconds;
    }
    if (minutes < 10) {
      minutes = "0" + minutes
    }
    return minutes + ':' + seconds;
  }
}
