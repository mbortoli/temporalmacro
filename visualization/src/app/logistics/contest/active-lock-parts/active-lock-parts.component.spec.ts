import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActiveLockPartsComponent } from './active-lock-parts.component';

describe('ActiveLockPartsComponent', () => {
  let component: ActiveLockPartsComponent;
  let fixture: ComponentFixture<ActiveLockPartsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActiveLockPartsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActiveLockPartsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
