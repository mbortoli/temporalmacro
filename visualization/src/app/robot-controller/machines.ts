export const MachineOptions = [
  {value: "C-CS1", label: "C-CS1"},
  {value: "C-CS2", label: "C-CS2"},
  {value: "C-RS1", label: "C-RS1"},
  {value: "C-RS2", label: "C-RS2"},
  {value: "C-BS", label: "C-BS"},
  {value: "C-DS", label: "C-DS"},
  {value: "C-SS", label: "C-SS"},

  {value: "M-CS1", label: "M-CS1"},
  {value: "M-CS2", label: "M-CS2"},
  {value: "M-RS1", label: "M-RS1"},
  {value: "M-RS2", label: "M-RS2"},
  {value: "M-BS", label: "M-BS"},
  {value: "M-DS", label: "M-DS"},
  {value: "M-SS", label: "M-SS"},
];
