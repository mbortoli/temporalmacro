import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RobotControllerComponent } from './robot-controller.component';

describe('RobotControllerComponent', () => {
  let component: RobotControllerComponent;
  let fixture: ComponentFixture<RobotControllerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RobotControllerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RobotControllerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
