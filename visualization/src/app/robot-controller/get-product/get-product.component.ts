import { Component, OnInit } from '@angular/core';
import {FormGroup} from "@angular/forms";
import {formly} from "ngx-formly-helpers";
import {EndpointService} from "../../services/endpoint.service";
import {RobotOptions} from "../robots";
import {MachineOptions} from "../machines";
import {MachinePointOptions} from "../machine-points";

@Component({
  selector: 'app-get-product',
  templateUrl: './get-product.component.html',
  styleUrls: ['./get-product.component.sass']
})
export class GetProductComponent implements OnInit {

  form = new FormGroup({});
  model: any = {};
  fields: any[] = [
    formly.requiredSelect("robotId", "Robot ID", RobotOptions),
    formly.requiredSelect("machine", "Machine", MachineOptions),
    formly.requiredSelect("machinePoint", "MachinePoint", MachinePointOptions),
  ];

  constructor(private endpoint: EndpointService) { }

  ngOnInit(): void {
  }

  public submit() {
    console.log("Model is: ", this.model);
    this.endpoint.post("robot-controller/get-product", this.model)
      .subscribe(res => console.log("Get Product send!"));
  }
}
