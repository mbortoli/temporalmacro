import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PauseTaskComponent } from './pause-task.component';

describe('PauseTaskComponent', () => {
  let component: PauseTaskComponent;
  let fixture: ComponentFixture<PauseTaskComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PauseTaskComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PauseTaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
