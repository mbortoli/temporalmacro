import {Component, OnInit} from '@angular/core';
import {FormGroup} from "@angular/forms";
import {formly} from "ngx-formly-helpers";
import {EndpointService} from "../../services/endpoint.service";
import {RobotOptions} from "../robots";

@Component({
  selector: 'app-move-to-waypoint',
  templateUrl: './move-to-waypoint.component.html',
  styleUrls: ['./move-to-waypoint.component.sass']
})
export class MoveToWaypointComponent implements OnInit {

  form = new FormGroup({});
  model: any = {};
  fields: any[] = [
    formly.requiredSelect("robotId", "Robot ID", RobotOptions),
    formly.requiredText("zone", "Zone")
  ];

  constructor(private endpoint: EndpointService) {
  }

  ngOnInit(): void {
  }

  submit(): void {
    console.log("Model is: ", this.model);
    this.endpoint.post("robot-controller/move-to", this.model)
      .subscribe(res => console.log("Go To send!"));
  }
}
