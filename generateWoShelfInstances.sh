#!/bin/bash
FILES="./macroinstances/*"
for f in $FILES
do
  echo "Processing $f file..."
  # take action on each file. $f store current file name
  basename "$f"
  g="$(basename -- $f)"
  #echo $f
  #echo "$g"
  python cleanShelf.py  $f "./woshelfinstances/$g"
  #cat "$f"
done
