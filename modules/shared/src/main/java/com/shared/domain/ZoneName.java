package com.shared.domain;

import com.grips.model.teamserver.Zone;
import lombok.Getter;
import lombok.Setter;
import org.robocup_logistics.llsf_msgs.ZoneProtos;

@Getter
@Setter
public class ZoneName {
    private final String rawZone;

    public ZoneName(String zone) {
        this.rawZone = zone;
    }

    public boolean isMagenta() {
        return this.rawZone.charAt(0) == 'M';
    }

    public boolean isCyan() {
        return this.rawZone.charAt(0) == 'M';
    }

    public ZoneName mirror() {
        if (this.isMagenta()) {
            return new ZoneName(this.rawZone.replace("M", "C"));
        } else {
            return new ZoneName(this.rawZone.replace("C", "M"));
        }
    }

    public ZoneProtos.Zone toProto() {
        return ZoneProtos.Zone.valueOf(this.rawZone);
    }
}
