package com.shared.domain;

public enum RingColor {
    RING_BLUE,
    RING_GREEN,
    RING_ORANGE,
    RING_YELLOW
}
