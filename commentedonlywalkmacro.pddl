(define (domain driverlog)
  (:requirements :typing :durative-actions :fluents) 
  (:types           location locatable - object
		driver truck obj - locatable)

  (:predicates 
(posaux_at  ?obj - locatable ?loc - location)
(negaux_at  ?obj - locatable ?loc - location)
(posaux_empty  ?v - truck)
(negaux_empty  ?v - truck)
		(at ?obj - locatable ?loc - location)
		(in ?obj1 - obj ?obj - truck)
		(driving ?d - driver ?v - truck)
		(link ?x ?y - location) (path ?x ?y - location)
		(empty ?v - truck)
)

(:functions
    (pathdistance ?x - location ?y - location) 
)

  (:durative-action walk_board-truck
   :parameters ( ?driver - DRIVER  ?loc-from - LOCATION  ?loc - LOCATION  ?truck - TRUCK )
   :duration (= ?duration ( + (pathdistance ?loc-from ?loc ) 1) ) 
   :condition (and 
   (at start (path ?loc-from ?loc))
   (at start (empty ?truck))
   (at start (at ?driver ?loc-from))
   (at start (negaux_at ?driver ?loc))
   (at start (negaux_at ?driver ?loc-from))
   (over all (at ?truck ?loc))
   (at start (posaux_empty ?truck))
   (at start (posaux_at ?driver ?loc))
   (at start (negaux_empty ?truck))
   )
   :effect (and 
   (at start (not (empty ?truck)))
   ;(at start (not (at ?driver ?loc)))
   (at start (not (at ?driver ?loc-from)))
   (at start (not (posaux_empty ?truck)))
   (at start (not (posaux_at ?driver ?loc)))
   (at start (not (negaux_empty ?truck)))
   (at end (driving ?driver ?truck))
   (at end (posaux_empty ?truck))
   (at end (posaux_at ?driver ?loc))
   (at end (negaux_empty ?truck))
   )
)
  (:durative-action disembark-truck_walk
   :parameters ( ?driver - DRIVER  ?truck - TRUCK  ?loc-from - LOCATION  ?loc-to - LOCATION )
   :duration (= ?duration ( + (pathdistance ?loc-from ?loc-to ) 1) ) 
   :condition (and 
   (at start (driving ?driver ?truck))
   (over all (path ?loc-from ?loc-to))
   (over all (at ?truck ?loc-from))
   (at end (posaux_empty ?truck))
   (at start (posaux_at ?driver ?loc-from))
   (at start (negaux_empty ?truck))
   (at start (negaux_at ?driver ?loc-from))
   )
   :effect (and 
   (at start (not (driving ?driver ?truck)))
   ;(at start (not (at ?driver ?loc-from)))
   (at start (not (posaux_at ?driver ?loc-from)))
   (at start (not (negaux_empty ?truck)))
   (at start (not (negaux_at ?driver ?loc-from)))
   (at end (at ?driver ?loc-to))
   (at end (empty ?truck))
   (at end (posaux_at ?driver ?loc-from))
   (at end (negaux_empty ?truck))
   (at end (negaux_at ?driver ?loc-from))
   )
)
  (:durative-action load-truck
   :parameters ( ?obj - OBJ  ?truck - TRUCK  ?loc - LOCATION )
   :duration (= ?duration 10 ) 
   :condition (and 
   (at start (at ?obj ?loc))
   (at start (negaux_at ?obj ?loc))
   (over all (at ?truck ?loc))
   )
   :effect (and 
   (at start (not (at ?obj ?loc)))
   (at end (in ?obj ?truck))
   )
)
  (:durative-action unload-truck
   :parameters ( ?obj - OBJ  ?truck - TRUCK  ?loc - LOCATION )
   :duration (= ?duration 10 ) 
   :condition (and 
   (at start (in ?obj ?truck))
   (over all (at ?truck ?loc))
   (at end (posaux_at ?obj ?loc))
   )
   :effect (and 
   (at start (not (in ?obj ?truck)))
   (at end (at ?obj ?loc))
   )
)
  (:durative-action drive-truck
   :parameters ( ?truck - TRUCK  ?loc-from - LOCATION  ?loc-to - LOCATION  ?driver - DRIVER )
   :duration (= ?duration 10 ) 
   :condition (and 
   (at start (link ?loc-from ?loc-to))
   (at start (at ?truck ?loc-from))
   (at start (negaux_at ?truck ?loc-from))
   (over all (driving ?driver ?truck))
   (at end (posaux_at ?truck ?loc-to))
   )
   :effect (and 
   (at start (not (at ?truck ?loc-from)))
   (at end (at ?truck ?loc-to))
   )
)

 
)
