(define (problem task)
(:domain tdomain)
(:objects
    start - starting
    l1 l3 l5 - getlocation
    l2 l4 l6 - deliverlocation
    r1 r2 - robot
)

(:init
   (at r1 start)
   (free l1)
   (free l2)
   (free l3)
   (free l4)
   (at r2 l5)
   (free l6)
   (empty r1)
)

(:goal (and
   ;(level r1)
    (at r1 l1)
    (at r2 l2)
)
)

)