#!/bin/bash
FILES="./allspinstances/*"
for f in $FILES
do
  echo "Processing $f file..."
  # take action on each file. $f store current file name
  basename "$f"
  g="$(basename -- $f)"
  #echo $f
  #echo "$g"
  python genProblem.py driverlog.pddl $f "./macroinstances/$g"
  #cat "$f"
done
