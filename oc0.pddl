(define (problem task)
(:domain rcll)
(:objects
    start - starting
    bs - base_station
    ds - delivery_station
    cs1_input cs2_input - cap_station_input
    cs1_output cs2_output - cap_station_output
    cs1_shelf cs2_shelf - cap_station_shelf
    cs1 cs2 - cap_station
    rs1 rs2 - ring_station
    ;r1 r2 r3 - robot 
    r1 - robot
    p1 - product
    ;p2 p3 p4 - product
    black grey - ccolor
    yellow orange blue green - rcolor
    ;p1 p2 p3 p4 p5 p6 - product
)
(:init
   (at r1 start)
   ;(at r2 start)
   ;(at r3 start)
   (free r1)
   ;(free r2)
   ;(free r3)
   (empty bs)
   (empty cs1_input)
   (empty cs1_output)
   (empty cs1_shelf)
   (empty cs2_input)
   (empty cs2_output)
   (empty cs2_shelf)
   (empty ds)
   (empty rs1)
   (empty rs2)
   (stage_c0_0 p1)
   ;(stage_c0_0 p2)
   ;(stage_c0_0 p3)
   ;(stage_c0_0 p4)
   ;(stage_c0_0 p5)
   ;(stage_c0_0 p6)
   (n_holding r1)
   ;(n_holding r2)
   ;(n_holding r3)
   (parent_cs cs1_input cs1)
   (parent_cs cs1_output cs1)
   (parent_cs cs1_shelf cs1)
   (parent_cs cs2_input cs2)
   (parent_cs cs2_output cs2)
   (parent_cs cs2_shelf cs2)
   (n_station_holding cs1)
   (n_cs_station_has_capbase cs1)
   (n_station_holding cs2)
   (n_cs_station_has_capbase cs2)

   (capcolor p1 black)
   ;(capcolor p2 grey)
   ;(capcolor p3 black)
   ;(capcolor p4 grey)
   ;(capcolor p5 black)
   ;(capcolor p6 grey)

   ;(ring1color p1 yellow)
   ;(ring1color p2 orange)
   ;(ring1color p3 green)
   ;(ring1color p4 blue)
   ;(ring1color p5 blue)
   
   ;(ring2color p3 blue)
   ;(ring2color p4 green)

   ;(ring3color p4 orange)


   (stationcapcolor cs1 black)
   (stationcapcolor cs2 grey)

   (stationringcolor rs1 yellow)
   (stationringcolor rs1 orange)
   (stationringcolor rs2 green)
   (stationringcolor rs2 blue)

   (=(loadedbases rs1) 0)
   (=(loadedbases rs2) 0)

   (=(requiredbases yellow) 2)
   (=(requiredbases orange) 1)
   (=(requiredbases blue) 1)
   (=(requiredbases green) 0)
   
   ;(n_locked bs)
   ;(n_locked cs1)
   ;(n_locked ds)

   (= (complexity p1) 0)
   ;(= (complexity p2) 1)
   ;(= (complexity p3) 2)
   ;(= (complexity p4) 3)

   (= (distance start bs) 5)
   (= (distance start ds) 5)
   (= (distance start cs1_input) 5)
   (= (distance start cs1_output) 5)
   (= (distance start cs1_shelf) 5)
   (= (distance start rs1) 5)
   (= (distance start cs2_input) 5)
   (= (distance start cs2_output) 5)
   (= (distance start cs2_shelf) 5)
   (= (distance start rs2) 5)

   (= (distance bs start) 5)
   (= (distance bs ds) 5)
   (= (distance bs cs1_input) 5)
   (= (distance bs cs1_output) 5)
   (= (distance bs cs1_shelf) 5)
   (= (distance bs rs1) 5)
   (= (distance bs cs2_input) 5)
   (= (distance bs cs2_output) 5)
   (= (distance bs cs2_shelf) 5)
   (= (distance bs rs2) 5)

   (= (distance ds start) 5)
   (= (distance ds bs) 5)
   (= (distance ds cs1_input) 5)
   (= (distance ds cs1_output) 5)
   (= (distance ds cs1_shelf) 5)
   (= (distance ds rs1) 5)
   (= (distance ds cs2_input) 5)
   (= (distance ds cs2_output) 5)
   (= (distance ds cs2_shelf) 5)
   (= (distance ds rs2) 5)

   (= (distance cs1_input start) 5)
   (= (distance cs1_input ds) 5)
   (= (distance cs1_input bs) 5)
   (= (distance cs1_input cs1_output) 1)
   (= (distance cs1_input cs1_shelf) 1)
   (= (distance cs1_input rs1) 5)
   (= (distance cs1_input cs2_input) 5)
   (= (distance cs1_input cs2_output) 5)
   (= (distance cs1_input cs2_shelf) 5)
   (= (distance cs1_input rs2) 5)

   (= (distance cs1_output start) 5)
   (= (distance cs1_output ds) 5)
   (= (distance cs1_output bs) 5)
   (= (distance cs1_output cs1_input) 1)
   (= (distance cs1_output cs1_shelf) 1)
   (= (distance cs1_output rs1) 5)
   (= (distance cs1_output cs2_input) 5)
   (= (distance cs1_output cs2_output) 5)
   (= (distance cs1_output cs2_shelf) 1)
   (= (distance cs1_output rs2) 5)

   (= (distance cs1_shelf start) 5)
   (= (distance cs1_shelf ds) 5)
   (= (distance cs1_shelf bs) 5)
   (= (distance cs1_shelf cs1_input) 1)
   (= (distance cs1_shelf cs1_output) 1)
   (= (distance cs1_shelf rs1) 5)
   (= (distance cs1_shelf cs2_input) 5)
   (= (distance cs1_shelf cs2_output) 5)
   (= (distance cs1_shelf cs2_shelf) 1)
   (= (distance cs1_shelf rs2) 5)

   (= (distance rs1 start) 5)
   (= (distance rs1 ds) 5)
   (= (distance rs1 cs1_input) 5)
   (= (distance rs1 cs1_output) 5)
   (= (distance rs1 cs1_shelf) 5)
   (= (distance rs1 bs) 5)
   (= (distance rs1 cs2_input) 5)
   (= (distance rs1 cs2_output) 5)
   (= (distance rs1 cs2_shelf) 5)
   (= (distance rs1 rs2) 5)

   (= (distance cs2_input start) 5)
   (= (distance cs2_input ds) 5)
   (= (distance cs2_input bs) 5)
   (= (distance cs2_input cs1_output) 5)
   (= (distance cs2_input cs1_shelf) 5)
   (= (distance cs2_input rs1) 5)
   (= (distance cs2_input cs1_input) 5)
   (= (distance cs2_input cs2_output) 1)
   (= (distance cs2_input cs2_shelf) 1)
   (= (distance cs2_input rs2) 5)

   (= (distance cs2_output start) 5)
   (= (distance cs2_output ds) 5)
   (= (distance cs2_output bs) 5)
   (= (distance cs2_output cs1_input) 5)
   (= (distance cs2_output cs1_shelf) 5)
   (= (distance cs2_output rs1) 5)
   (= (distance cs2_output cs2_input) 5)
   (= (distance cs2_output cs1_output) 1)
   (= (distance cs2_output cs2_shelf) 1)
   (= (distance cs2_output rs2) 5)

   (= (distance cs2_shelf start) 5)
   (= (distance cs2_shelf ds) 5)
   (= (distance cs2_shelf bs) 5)
   (= (distance cs2_shelf cs1_input) 5)
   (= (distance cs2_shelf cs1_output) 5)
   (= (distance cs2_shelf rs1) 5)
   (= (distance cs2_shelf cs2_input) 5)
   (= (distance cs2_shelf cs2_output) 1)
   (= (distance cs2_shelf cs1_shelf) 1)
   (= (distance cs2_shelf rs2) 5)

   (= (distance rs2 start) 5)
   (= (distance rs2 ds) 5)
   (= (distance rs2 cs1_input) 5)
   (= (distance rs2 cs1_output) 5)
   (= (distance rs2 cs1_shelf) 5)
   (= (distance rs2 bs) 5)
   (= (distance rs2 cs2_input) 5)
   (= (distance rs2 cs2_output) 5)
   (= (distance rs2 cs2_shelf) 5)
   (= (distance rs2 rs1) 5)

   (= (distance rs2 rs2) 0)
   (= (distance rs1 rs1) 0)


   

)

;(:constraints 
;	(preference p1 (within 100 (stage_c0_4 p1))	)
;)
 

(:goal (and
   ;(stage_c0_1 p1)
   
   (delivered p1)
   ;(finish cs1)
   ;(ff cs1)
   ;(delivered p2)  
   ;(delivered p3)
   ;(delivered p4)
  ;(stage_c0_4 p5)
   ;(stage_c0_4 p6)
   
)
)

;(:metric minimize (+ (total-time) (* 1000 (is-violated p1)) ) )
(:metric minimize (total-time))

)
