(define (domain tdomain)
 (:requirements :strips :typing :durative-actions :fluents )
 (:types  getlocation deliverlocation starting - location
 robot)

 (:predicates 
     (at ?r - robot ?l - location)
     (free ?l - location)
     (empty ?r - robot)
     (holding ?r - robot)
     (level ?r - robot)
	 (negaux_free_?l)
(negaux_at_r_l)
(posaux_empty_r)
(posaux_holding_r)
(negaux_free_l)
(posaux_free_l)
(negaux_empty_r)
(negaux_holding_r)



 )



   (:durative-action move_get
   :parameters ( ?r - ROBOT  ?l1 - LOCATION  ?l - GETLOCATION )
   :duration (= ?duration 10 ) 
   :condition (and 
   (at start (at ?r ?l1))
   (at start (free ?l))
   (at start (empty ?r))
   (at start (posaux_empty_r))
   (at start (posaux_free_l))
   (at start (negaux_at_r_l))
   (at start (negaux_free_l))
   (at start (negaux_empty_r))
   )
   :effect (and 
   (at start (free ?l1))
   (at start (not (at ?r ?l1)))
   (at start (not (free ?l)))
   (at start (not (empty ?r)))
   (at start (not (posaux_empty_r)))
   (at start (not (posaux_free_l)))
   (at start (not (negaux_at_r_l)))
   (at start (not (negaux_free_l)))
   (at start (not (negaux_empty_r)))
   (at end (at ?r ?l))
   (at end (holding ?r))
   (at end (posaux_empty_r))
   (at end (posaux_free_l))
   (at end (negaux_at_r_l))
   (at end (negaux_free_l))
   (at end (negaux_empty_r))
   )
)
  (:durative-action move_deliver
   :parameters ( ?r - ROBOT  ?l1 - LOCATION  ?l - DELIVERLOCATION )
   :duration (= ?duration 10 ) 
   :condition (and 
   (at start (at ?r ?l1))
   (at start (free ?l))
   (at start (holding ?r))
   (at start (posaux_holding_r))
   (at start (posaux_free_l))
   (at start (negaux_at_r_l))
   (at start (negaux_free_l))
   (at start (negaux_holding_r))
   )
   :effect (and 
   (at start (free ?l1))
   (at start (not (at ?r ?l1)))
   (at start (not (free ?l)))
   (at start (not (holding ?r)))
   (at start (not (posaux_holding_r)))
   (at start (not (posaux_free_l)))
   (at start (not (negaux_at_r_l)))
   (at start (not (negaux_free_l)))
   (at start (not (negaux_holding_r)))
   (at end (at ?r ?l))
   (at end (level ?r))
   (at end (empty ?r))
   (at end (posaux_holding_r))
   (at end (posaux_free_l))
   (at end (negaux_at_r_l))
   (at end (negaux_free_l))
   (at end (negaux_holding_r))
   )
)





)
