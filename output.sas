begin_variables
15
var0 8 -1
var1 8 -1
var2 3 -1
var3 2 -1
var4 2 -1
var5 2 -1
var6 2 -1
var7 2 -1
var8 2 -1
var9 2 -1
var10 2 -1
var11 -1 -1
var12 -1 -1
var13 -1 -1
var14 -1 -1
end_variables
begin_state
6
4
0
1
0
0
0
0
1
0
1
10.0
30.0
1.0
0.0
end_state
begin_goal
2
0 0
1 1
end_goal
104
begin_operator
deliver r1 l2
= 12
1
0 1
1
0 1
0
1
0 0 0 2 1 2
2
0 0 0 2 2 0
0 0 0 14 + 13
end_operator
begin_operator
deliver r1 l4
= 12
1
0 3
1
0 3
0
1
0 0 0 2 1 2
2
0 0 0 2 2 0
0 0 0 14 + 13
end_operator
begin_operator
deliver r1 l6
= 12
1
0 5
1
0 5
0
1
0 0 0 2 1 2
2
0 0 0 2 2 0
0 0 0 14 + 13
end_operator
begin_operator
get r1 l1
= 12
1
0 0
1
0 0
0
1
0 0 0 2 0 2
1
0 0 0 2 2 1
end_operator
begin_operator
get r1 l3
= 12
1
0 2
1
0 2
0
1
0 0 0 2 0 2
1
0 0 0 2 2 1
end_operator
begin_operator
get r1 l5
= 12
1
0 4
1
0 4
0
1
0 0 0 2 0 2
1
0 0 0 2 2 1
end_operator
begin_operator
move r1 l1 l1
= 11
0
0
0
2
0 0 0 0 0 7
0 0 0 4 -1 0
2
0 0 0 0 7 0
0 0 0 4 0 1
end_operator
begin_operator
move r1 l1 l2
= 11
0
0
0
2
0 0 0 0 0 7
0 0 0 4 -1 0
2
0 0 0 0 7 1
0 0 0 5 0 1
end_operator
begin_operator
move r1 l1 l3
= 11
0
0
0
2
0 0 0 0 0 7
0 0 0 4 -1 0
2
0 0 0 0 7 2
0 0 0 6 0 1
end_operator
begin_operator
move r1 l1 l4
= 11
0
0
0
2
0 0 0 0 0 7
0 0 0 4 -1 0
2
0 0 0 0 7 3
0 0 0 7 0 1
end_operator
begin_operator
move r1 l1 l5
= 11
0
0
0
2
0 0 0 0 0 7
0 0 0 4 -1 0
2
0 0 0 0 7 4
0 0 0 8 0 1
end_operator
begin_operator
move r1 l1 l6
= 11
0
0
0
2
0 0 0 0 0 7
0 0 0 4 -1 0
2
0 0 0 0 7 5
0 0 0 9 0 1
end_operator
begin_operator
move r1 l1 start
= 11
0
0
0
2
0 0 0 0 0 7
0 0 0 4 -1 0
2
0 0 0 0 7 6
0 0 0 10 0 1
end_operator
begin_operator
move r1 l2 l1
= 11
0
0
0
2
0 0 0 0 1 7
0 0 0 5 -1 0
2
0 0 0 0 7 0
0 0 0 4 0 1
end_operator
begin_operator
move r1 l2 l2
= 11
0
0
0
2
0 0 0 0 1 7
0 0 0 5 -1 0
2
0 0 0 0 7 1
0 0 0 5 0 1
end_operator
begin_operator
move r1 l2 l3
= 11
0
0
0
2
0 0 0 0 1 7
0 0 0 5 -1 0
2
0 0 0 0 7 2
0 0 0 6 0 1
end_operator
begin_operator
move r1 l2 l4
= 11
0
0
0
2
0 0 0 0 1 7
0 0 0 5 -1 0
2
0 0 0 0 7 3
0 0 0 7 0 1
end_operator
begin_operator
move r1 l2 l5
= 11
0
0
0
2
0 0 0 0 1 7
0 0 0 5 -1 0
2
0 0 0 0 7 4
0 0 0 8 0 1
end_operator
begin_operator
move r1 l2 l6
= 11
0
0
0
2
0 0 0 0 1 7
0 0 0 5 -1 0
2
0 0 0 0 7 5
0 0 0 9 0 1
end_operator
begin_operator
move r1 l2 start
= 11
0
0
0
2
0 0 0 0 1 7
0 0 0 5 -1 0
2
0 0 0 0 7 6
0 0 0 10 0 1
end_operator
begin_operator
move r1 l3 l1
= 11
0
0
0
2
0 0 0 0 2 7
0 0 0 6 -1 0
2
0 0 0 0 7 0
0 0 0 4 0 1
end_operator
begin_operator
move r1 l3 l2
= 11
0
0
0
2
0 0 0 0 2 7
0 0 0 6 -1 0
2
0 0 0 0 7 1
0 0 0 5 0 1
end_operator
begin_operator
move r1 l3 l3
= 11
0
0
0
2
0 0 0 0 2 7
0 0 0 6 -1 0
2
0 0 0 0 7 2
0 0 0 6 0 1
end_operator
begin_operator
move r1 l3 l4
= 11
0
0
0
2
0 0 0 0 2 7
0 0 0 6 -1 0
2
0 0 0 0 7 3
0 0 0 7 0 1
end_operator
begin_operator
move r1 l3 l5
= 11
0
0
0
2
0 0 0 0 2 7
0 0 0 6 -1 0
2
0 0 0 0 7 4
0 0 0 8 0 1
end_operator
begin_operator
move r1 l3 l6
= 11
0
0
0
2
0 0 0 0 2 7
0 0 0 6 -1 0
2
0 0 0 0 7 5
0 0 0 9 0 1
end_operator
begin_operator
move r1 l3 start
= 11
0
0
0
2
0 0 0 0 2 7
0 0 0 6 -1 0
2
0 0 0 0 7 6
0 0 0 10 0 1
end_operator
begin_operator
move r1 l4 l1
= 11
0
0
0
2
0 0 0 0 3 7
0 0 0 7 -1 0
2
0 0 0 0 7 0
0 0 0 4 0 1
end_operator
begin_operator
move r1 l4 l2
= 11
0
0
0
2
0 0 0 0 3 7
0 0 0 7 -1 0
2
0 0 0 0 7 1
0 0 0 5 0 1
end_operator
begin_operator
move r1 l4 l3
= 11
0
0
0
2
0 0 0 0 3 7
0 0 0 7 -1 0
2
0 0 0 0 7 2
0 0 0 6 0 1
end_operator
begin_operator
move r1 l4 l4
= 11
0
0
0
2
0 0 0 0 3 7
0 0 0 7 -1 0
2
0 0 0 0 7 3
0 0 0 7 0 1
end_operator
begin_operator
move r1 l4 l5
= 11
0
0
0
2
0 0 0 0 3 7
0 0 0 7 -1 0
2
0 0 0 0 7 4
0 0 0 8 0 1
end_operator
begin_operator
move r1 l4 l6
= 11
0
0
0
2
0 0 0 0 3 7
0 0 0 7 -1 0
2
0 0 0 0 7 5
0 0 0 9 0 1
end_operator
begin_operator
move r1 l4 start
= 11
0
0
0
2
0 0 0 0 3 7
0 0 0 7 -1 0
2
0 0 0 0 7 6
0 0 0 10 0 1
end_operator
begin_operator
move r1 l5 l1
= 11
0
0
0
2
0 0 0 8 -1 0
0 0 0 0 4 7
2
0 0 0 0 7 0
0 0 0 4 0 1
end_operator
begin_operator
move r1 l5 l2
= 11
0
0
0
2
0 0 0 8 -1 0
0 0 0 0 4 7
2
0 0 0 0 7 1
0 0 0 5 0 1
end_operator
begin_operator
move r1 l5 l3
= 11
0
0
0
2
0 0 0 8 -1 0
0 0 0 0 4 7
2
0 0 0 0 7 2
0 0 0 6 0 1
end_operator
begin_operator
move r1 l5 l4
= 11
0
0
0
2
0 0 0 8 -1 0
0 0 0 0 4 7
2
0 0 0 0 7 3
0 0 0 7 0 1
end_operator
begin_operator
move r1 l5 l5
= 11
0
0
0
2
0 0 0 8 -1 0
0 0 0 0 4 7
2
0 0 0 0 7 4
0 0 0 8 0 1
end_operator
begin_operator
move r1 l5 l6
= 11
0
0
0
2
0 0 0 8 -1 0
0 0 0 0 4 7
2
0 0 0 0 7 5
0 0 0 9 0 1
end_operator
begin_operator
move r1 l5 start
= 11
0
0
0
2
0 0 0 8 -1 0
0 0 0 0 4 7
2
0 0 0 0 7 6
0 0 0 10 0 1
end_operator
begin_operator
move r1 l6 l1
= 11
0
0
0
2
0 0 0 0 5 7
0 0 0 9 -1 0
2
0 0 0 0 7 0
0 0 0 4 0 1
end_operator
begin_operator
move r1 l6 l2
= 11
0
0
0
2
0 0 0 0 5 7
0 0 0 9 -1 0
2
0 0 0 0 7 1
0 0 0 5 0 1
end_operator
begin_operator
move r1 l6 l3
= 11
0
0
0
2
0 0 0 0 5 7
0 0 0 9 -1 0
2
0 0 0 0 7 2
0 0 0 6 0 1
end_operator
begin_operator
move r1 l6 l4
= 11
0
0
0
2
0 0 0 0 5 7
0 0 0 9 -1 0
2
0 0 0 0 7 3
0 0 0 7 0 1
end_operator
begin_operator
move r1 l6 l5
= 11
0
0
0
2
0 0 0 0 5 7
0 0 0 9 -1 0
2
0 0 0 0 7 4
0 0 0 8 0 1
end_operator
begin_operator
move r1 l6 l6
= 11
0
0
0
2
0 0 0 0 5 7
0 0 0 9 -1 0
2
0 0 0 0 7 5
0 0 0 9 0 1
end_operator
begin_operator
move r1 l6 start
= 11
0
0
0
2
0 0 0 0 5 7
0 0 0 9 -1 0
2
0 0 0 0 7 6
0 0 0 10 0 1
end_operator
begin_operator
move r1 start l1
= 11
0
0
0
2
0 0 0 0 6 7
0 0 0 10 -1 0
2
0 0 0 0 7 0
0 0 0 4 0 1
end_operator
begin_operator
move r1 start l2
= 11
0
0
0
2
0 0 0 0 6 7
0 0 0 10 -1 0
2
0 0 0 0 7 1
0 0 0 5 0 1
end_operator
begin_operator
move r1 start l3
= 11
0
0
0
2
0 0 0 0 6 7
0 0 0 10 -1 0
2
0 0 0 0 7 2
0 0 0 6 0 1
end_operator
begin_operator
move r1 start l4
= 11
0
0
0
2
0 0 0 0 6 7
0 0 0 10 -1 0
2
0 0 0 0 7 3
0 0 0 7 0 1
end_operator
begin_operator
move r1 start l5
= 11
0
0
0
2
0 0 0 0 6 7
0 0 0 10 -1 0
2
0 0 0 0 7 4
0 0 0 8 0 1
end_operator
begin_operator
move r1 start l6
= 11
0
0
0
2
0 0 0 0 6 7
0 0 0 10 -1 0
2
0 0 0 0 7 5
0 0 0 9 0 1
end_operator
begin_operator
move r1 start start
= 11
0
0
0
2
0 0 0 0 6 7
0 0 0 10 -1 0
2
0 0 0 0 7 6
0 0 0 10 0 1
end_operator
begin_operator
move r2 l1 l1
= 11
0
0
0
2
0 0 0 1 0 7
0 0 0 4 -1 0
2
0 0 0 1 7 0
0 0 0 4 0 1
end_operator
begin_operator
move r2 l1 l2
= 11
0
0
0
2
0 0 0 1 0 7
0 0 0 4 -1 0
2
0 0 0 1 7 1
0 0 0 5 0 1
end_operator
begin_operator
move r2 l1 l3
= 11
0
0
0
2
0 0 0 1 0 7
0 0 0 4 -1 0
2
0 0 0 1 7 2
0 0 0 6 0 1
end_operator
begin_operator
move r2 l1 l4
= 11
0
0
0
2
0 0 0 1 0 7
0 0 0 4 -1 0
2
0 0 0 1 7 3
0 0 0 7 0 1
end_operator
begin_operator
move r2 l1 l5
= 11
0
0
0
2
0 0 0 1 0 7
0 0 0 4 -1 0
2
0 0 0 8 0 1
0 0 0 1 7 4
end_operator
begin_operator
move r2 l1 l6
= 11
0
0
0
2
0 0 0 1 0 7
0 0 0 4 -1 0
2
0 0 0 1 7 5
0 0 0 9 0 1
end_operator
begin_operator
move r2 l1 start
= 11
0
0
0
2
0 0 0 1 0 7
0 0 0 4 -1 0
2
0 0 0 1 7 6
0 0 0 10 0 1
end_operator
begin_operator
move r2 l2 l1
= 11
0
0
0
2
0 0 0 1 1 7
0 0 0 5 -1 0
2
0 0 0 1 7 0
0 0 0 4 0 1
end_operator
begin_operator
move r2 l2 l2
= 11
0
0
0
2
0 0 0 1 1 7
0 0 0 5 -1 0
2
0 0 0 1 7 1
0 0 0 5 0 1
end_operator
begin_operator
move r2 l2 l3
= 11
0
0
0
2
0 0 0 1 1 7
0 0 0 5 -1 0
2
0 0 0 1 7 2
0 0 0 6 0 1
end_operator
begin_operator
move r2 l2 l4
= 11
0
0
0
2
0 0 0 1 1 7
0 0 0 5 -1 0
2
0 0 0 1 7 3
0 0 0 7 0 1
end_operator
begin_operator
move r2 l2 l5
= 11
0
0
0
2
0 0 0 1 1 7
0 0 0 5 -1 0
2
0 0 0 8 0 1
0 0 0 1 7 4
end_operator
begin_operator
move r2 l2 l6
= 11
0
0
0
2
0 0 0 1 1 7
0 0 0 5 -1 0
2
0 0 0 1 7 5
0 0 0 9 0 1
end_operator
begin_operator
move r2 l2 start
= 11
0
0
0
2
0 0 0 1 1 7
0 0 0 5 -1 0
2
0 0 0 1 7 6
0 0 0 10 0 1
end_operator
begin_operator
move r2 l3 l1
= 11
0
0
0
2
0 0 0 1 2 7
0 0 0 6 -1 0
2
0 0 0 1 7 0
0 0 0 4 0 1
end_operator
begin_operator
move r2 l3 l2
= 11
0
0
0
2
0 0 0 1 2 7
0 0 0 6 -1 0
2
0 0 0 1 7 1
0 0 0 5 0 1
end_operator
begin_operator
move r2 l3 l3
= 11
0
0
0
2
0 0 0 1 2 7
0 0 0 6 -1 0
2
0 0 0 1 7 2
0 0 0 6 0 1
end_operator
begin_operator
move r2 l3 l4
= 11
0
0
0
2
0 0 0 1 2 7
0 0 0 6 -1 0
2
0 0 0 1 7 3
0 0 0 7 0 1
end_operator
begin_operator
move r2 l3 l5
= 11
0
0
0
2
0 0 0 1 2 7
0 0 0 6 -1 0
2
0 0 0 8 0 1
0 0 0 1 7 4
end_operator
begin_operator
move r2 l3 l6
= 11
0
0
0
2
0 0 0 1 2 7
0 0 0 6 -1 0
2
0 0 0 1 7 5
0 0 0 9 0 1
end_operator
begin_operator
move r2 l3 start
= 11
0
0
0
2
0 0 0 1 2 7
0 0 0 6 -1 0
2
0 0 0 1 7 6
0 0 0 10 0 1
end_operator
begin_operator
move r2 l4 l1
= 11
0
0
0
2
0 0 0 1 3 7
0 0 0 7 -1 0
2
0 0 0 1 7 0
0 0 0 4 0 1
end_operator
begin_operator
move r2 l4 l2
= 11
0
0
0
2
0 0 0 1 3 7
0 0 0 7 -1 0
2
0 0 0 1 7 1
0 0 0 5 0 1
end_operator
begin_operator
move r2 l4 l3
= 11
0
0
0
2
0 0 0 1 3 7
0 0 0 7 -1 0
2
0 0 0 1 7 2
0 0 0 6 0 1
end_operator
begin_operator
move r2 l4 l4
= 11
0
0
0
2
0 0 0 1 3 7
0 0 0 7 -1 0
2
0 0 0 1 7 3
0 0 0 7 0 1
end_operator
begin_operator
move r2 l4 l5
= 11
0
0
0
2
0 0 0 1 3 7
0 0 0 7 -1 0
2
0 0 0 8 0 1
0 0 0 1 7 4
end_operator
begin_operator
move r2 l4 l6
= 11
0
0
0
2
0 0 0 1 3 7
0 0 0 7 -1 0
2
0 0 0 1 7 5
0 0 0 9 0 1
end_operator
begin_operator
move r2 l4 start
= 11
0
0
0
2
0 0 0 1 3 7
0 0 0 7 -1 0
2
0 0 0 1 7 6
0 0 0 10 0 1
end_operator
begin_operator
move r2 l5 l1
= 11
0
0
0
2
0 0 0 8 -1 0
0 0 0 1 4 7
2
0 0 0 1 7 0
0 0 0 4 0 1
end_operator
begin_operator
move r2 l5 l2
= 11
0
0
0
2
0 0 0 8 -1 0
0 0 0 1 4 7
2
0 0 0 1 7 1
0 0 0 5 0 1
end_operator
begin_operator
move r2 l5 l3
= 11
0
0
0
2
0 0 0 8 -1 0
0 0 0 1 4 7
2
0 0 0 1 7 2
0 0 0 6 0 1
end_operator
begin_operator
move r2 l5 l4
= 11
0
0
0
2
0 0 0 8 -1 0
0 0 0 1 4 7
2
0 0 0 1 7 3
0 0 0 7 0 1
end_operator
begin_operator
move r2 l5 l5
= 11
0
0
0
2
0 0 0 8 -1 0
0 0 0 1 4 7
2
0 0 0 8 0 1
0 0 0 1 7 4
end_operator
begin_operator
move r2 l5 l6
= 11
0
0
0
2
0 0 0 8 -1 0
0 0 0 1 4 7
2
0 0 0 1 7 5
0 0 0 9 0 1
end_operator
begin_operator
move r2 l5 start
= 11
0
0
0
2
0 0 0 8 -1 0
0 0 0 1 4 7
2
0 0 0 1 7 6
0 0 0 10 0 1
end_operator
begin_operator
move r2 l6 l1
= 11
0
0
0
2
0 0 0 9 -1 0
0 0 0 1 5 7
2
0 0 0 1 7 0
0 0 0 4 0 1
end_operator
begin_operator
move r2 l6 l2
= 11
0
0
0
2
0 0 0 9 -1 0
0 0 0 1 5 7
2
0 0 0 1 7 1
0 0 0 5 0 1
end_operator
begin_operator
move r2 l6 l3
= 11
0
0
0
2
0 0 0 9 -1 0
0 0 0 1 5 7
2
0 0 0 1 7 2
0 0 0 6 0 1
end_operator
begin_operator
move r2 l6 l4
= 11
0
0
0
2
0 0 0 9 -1 0
0 0 0 1 5 7
2
0 0 0 1 7 3
0 0 0 7 0 1
end_operator
begin_operator
move r2 l6 l5
= 11
0
0
0
2
0 0 0 9 -1 0
0 0 0 1 5 7
2
0 0 0 8 0 1
0 0 0 1 7 4
end_operator
begin_operator
move r2 l6 l6
= 11
0
0
0
2
0 0 0 9 -1 0
0 0 0 1 5 7
2
0 0 0 1 7 5
0 0 0 9 0 1
end_operator
begin_operator
move r2 l6 start
= 11
0
0
0
2
0 0 0 9 -1 0
0 0 0 1 5 7
2
0 0 0 1 7 6
0 0 0 10 0 1
end_operator
begin_operator
move r2 start l1
= 11
0
0
0
2
0 0 0 1 6 7
0 0 0 10 -1 0
2
0 0 0 1 7 0
0 0 0 4 0 1
end_operator
begin_operator
move r2 start l2
= 11
0
0
0
2
0 0 0 1 6 7
0 0 0 10 -1 0
2
0 0 0 1 7 1
0 0 0 5 0 1
end_operator
begin_operator
move r2 start l3
= 11
0
0
0
2
0 0 0 1 6 7
0 0 0 10 -1 0
2
0 0 0 1 7 2
0 0 0 6 0 1
end_operator
begin_operator
move r2 start l4
= 11
0
0
0
2
0 0 0 1 6 7
0 0 0 10 -1 0
2
0 0 0 1 7 3
0 0 0 7 0 1
end_operator
begin_operator
move r2 start l5
= 11
0
0
0
2
0 0 0 1 6 7
0 0 0 10 -1 0
2
0 0 0 8 0 1
0 0 0 1 7 4
end_operator
begin_operator
move r2 start l6
= 11
0
0
0
2
0 0 0 1 6 7
0 0 0 10 -1 0
2
0 0 0 1 7 5
0 0 0 9 0 1
end_operator
begin_operator
move r2 start start
= 11
0
0
0
2
0 0 0 1 6 7
0 0 0 10 -1 0
2
0 0 0 1 7 6
0 0 0 10 0 1
end_operator
0
0
0
0
