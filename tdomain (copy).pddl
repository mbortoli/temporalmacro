(define (domain tdomain)
 (:requirements :strips :typing :durative-actions :fluents )
 (:types  getlocation deliverlocation starting - location
 robot)

 (:predicates 
     (at ?r - robot ?l - location)
     (free ?l - location)
     (empty ?r - robot)
     (holding ?r - robot)
     (level ?r - robot)

 )



  (:durative-action move 
	:parameters (?r - robot ?l1 ?l2 - location)
	:duration (= ?duration  10 ) 
	:condition (and 
		(at start (at ?r ?l1))
		(at end (free ?l2))
	)
	:effect (and
		(at start (free ?l1))
		(at start (not (at ?r ?l1)))
		(at end (at ?r ?l2))
		(at end (not (free ?l2)))
 	)
)

(:durative-action get 
	:parameters (?r - robot ?l - getlocation)
	:duration (= ?duration  30 ) 
	:condition (and 
		(at start (at ?r ?l))
        (over all (at ?r ?l))
		(at start (empty ?r))
	)
	:effect (and
		(at start (not (empty ?r)))
		(at end (holding ?r))

 	)
)

(:durative-action deliver 
	:parameters (?r - robot ?l - deliverlocation)
	:duration (= ?duration  30 ) 
	:condition (and 
		(at start (at ?r ?l))
        (over all (at ?r ?l))
		(at start (holding ?r))
	)
	:effect (and
		(at start (not (holding ?r)))
		(at end (empty ?r))
        (at end (level ?r))

 	)
)



)
