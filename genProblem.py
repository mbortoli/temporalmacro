#!/usr/bin/python

import sys, getopt



def buildHierar(typesfile):
   reading = open(typesfile,'r')
   lines = reading.readlines()
   typeHierar = {}
   typelist = []
   
   
   for line in lines:
      typelist = line.strip()[1:][:-1].strip().split(",")
      didSomething = False
      first = True
      alreadyChecked = []
      while didSomething or first:
         didSomething = False;
         first = False
         for t1 in typelist:
            for t2 in typelist:
               if t1.strip()!="object" and t2.strip()!="object":
                  if t1.split("-")[0].strip() == t2.split("-")[1].strip().lower() and t1.split("-")[1].strip().lower() != "object" and not (t1 + " and " + t2) in alreadyChecked:
                     print "herreeee " +  t1.split("-")[0].strip() + "  and   " + t2.split("-")[1].strip().lower()
                     #typelist.remove(t2)
                     alreadyChecked.append(t1 + " and " + t2)
                     alreadyChecked.append(t2 + " and " + t1)
                     t2 = t2.split("-")[0].strip() + " - " + t1.split("-")[1].strip()
                     typelist.append(t2)
                     didSomething = True;
   typelist = set(typelist)
   return typelist
   
def subtype(typ,typelist):
   subtypelist = []
   for t in typelist:
      if t.strip() != "object":
         if typ == t.split("-")[1].lower().strip():
            subtypelist.append(t.split("-")[0].lower())
   return subtypelist
   
   
def createTuple (list1,list2):
   tuple = []
   for l1 in list1:
      for l2 in list2:
         tuple.append(l1 + " " + l2)
   return tuple
      


def main(argv):
   inputfile = argv[0]
   inputproblemfile = argv[1]
   outputfile = argv[2]
   #print(inputfile)
   reading = open("aux.txt",'r')
   #writing = open(outputfile,'w')
   aux = []
   lines = reading.readlines()
   for line in lines:
      aux.append(line[:-1])
   print(aux)
   reading = open(inputfile,'r')
   lines = reading.readlines()
   check = False
   newpredicates = []
   for line in lines:
      if line.strip():
         if check and ")" in line and not ("(" in line and ")" in line):
            check = False 
         elif check:
            #print "ehi" + line
            pd = line.split("(")[1].split(" ")[0]
            found = False;
            for a in aux:
               #print(a.split(" ")[0][8:])
               if a.split(" ")[0][8:] == pd:
                  npd = a.split(" ")[0] + " " + line.split("(")[1][len(pd):][:-1]
                  newpredicates.append(npd)
                  found = True;
            print newpredicates
      
         if "(:predicates" in line:
            check = True
   writing = open("macrodomain.pddl",'w')
   reading = open(inputfile,'r')
   lines = reading.readlines()
   for line in lines:
      writing.write(line)
      if "(:predicates" in line:   
         for pd in newpredicates:
            writing.write(pd + "\n")
 
 
   
                 
   typelist = buildHierar("types.txt")
   print typelist
   
   #ll = subtype("toylocation",typelist)
   #print ll
   
   reading = open(inputproblemfile,'r')
   lines = reading.readlines()
   check = False
   typeArray = {}
   for line in lines:
      if line.strip():
         if check and ")" in line and not ("(" in line and ")" in line):
            check = False 
         elif check:
            #print line
            typ = line.split(" - ")[1].replace(" ", "")[:-1]
            if typeArray.has_key(typ):
               typeArray[typ] = typeArray[typ] + line.strip().split(" - ")[0].strip().split(" ")
            else:
               typeArray[typ] = line.strip().split(" - ")[0].strip().split(" ")
         if "(:objects" in line:
            check = True       
      
   print typeArray
   
   print "I am here"
   
   
   
   groundedatoms = []
   for a in newpredicates:
      index = a.find("?")
      parameters = a[index:][:-1]
      parameters = parameters.strip().split("?")[1:]
      print "param of " + a
      print parameters
      groundedojects = []  #map between variables and all possible instantiations
      for p in parameters:
         print "starting with " + p
         param = p.split(" ")[0].strip()
         ptype = p.split(" ")[2].strip()
         subptype = subtype(ptype,typelist)
         subptype.append(ptype)
         collectedsubobjects = []
         for s1 in subptype:
            if typeArray.has_key(s1.strip()):
               for elem in typeArray[s1.strip()]:
                  collectedsubobjects.append(elem)
         #print collectedsubobjects
         groundedojects.append(collectedsubobjects)
      print "printing grounded for " + a
      print groundedojects
      
      first = True
      tuples = []
      
      for k in groundedojects:
         if first:
            tuples = k
            first = False
         else:
            tuples = createTuple(tuples,k)
      print "printing tuples for " + a
      print tuples
      
      for t in tuples:
         groundedatoms.append(a.split(" ")[0] + " " + t + ")")
      #print groundedatoms
      
      
   writing = open(outputfile,'w')
   reading = open(inputproblemfile,'r')
   lines = reading.readlines()
   for line in lines:
      writing.write(line)
      if "(:init" in line:
         for elem in groundedatoms:
            writing.write(elem + "\n")
            
            
         


if __name__ == "__main__":
   main(sys.argv[1:])
