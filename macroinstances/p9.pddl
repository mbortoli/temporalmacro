(define (problem strips-sat-x-1)
(:domain satellite)
(:objects
	satellite0 - satellite
	instrument0 - instrument
	satellite1 - satellite
	instrument1 - instrument
	instrument2 - instrument
	instrument3 - instrument
	instrument4 - instrument
	instrument5 - instrument
	instrument6 - instrument
	satellite2 - satellite
	instrument7 - instrument
	instrument8 - instrument
	instrument9 - instrument
	instrument10 - instrument
	instrument11 - instrument
	instrument12 - instrument
	instrument13 - instrument
	instrument14 - instrument
	instrument15 - instrument
	satellite3 - satellite
	instrument16 - instrument
	instrument17 - instrument
	instrument18 - instrument
	instrument19 - instrument
	instrument20 - instrument
	instrument21 - instrument
	satellite4 - satellite
	instrument22 - instrument
	instrument23 - instrument
	instrument24 - instrument
	instrument25 - instrument
	instrument26 - instrument
	instrument27 - instrument
	instrument28 - instrument
	instrument29 - instrument
	instrument30 - instrument
	instrument31 - instrument
	instrument32 - instrument
	thermograph10 - mode
	spectrograph9 - mode
	spectrograph2 - mode
	thermograph5 - mode
	image8 - mode
	spectrograph13 - mode
	spectrograph1 - mode
	thermograph6 - mode
	image4 - mode
	thermograph12 - mode
	image11 - mode
	infrared3 - mode
	image0 - mode
	thermograph7 - mode
	Star0 - direction
	GroundStation1 - direction
	Star13 - direction
	Star8 - direction
	Star7 - direction
	Star12 - direction
	Star10 - direction
	GroundStation2 - direction
	GroundStation6 - direction
	GroundStation9 - direction
	Star4 - direction
	GroundStation15 - direction
	Star3 - direction
	GroundStation17 - direction
	Star14 - direction
	Star11 - direction
	Star5 - direction
	Star16 - direction
	Phenomenon18 - direction
	Phenomenon19 - direction
	Planet20 - direction
	Star21 - direction
	Phenomenon22 - direction
	Planet23 - direction
	Phenomenon24 - direction
	Star25 - direction
	Planet26 - direction
	Star27 - direction
	Phenomenon28 - direction
	Star29 - direction
	Phenomenon30 - direction
	Phenomenon31 - direction
	Phenomenon32 - direction
	Star33 - direction
	Planet34 - direction
	Star35 - direction
	Planet36 - direction
	Phenomenon37 - direction
	Planet38 - direction
	Planet39 - direction
	Phenomenon40 - direction
	Planet41 - direction
	Phenomenon42 - direction
	Star43 - direction
	Phenomenon44 - direction
	Phenomenon45 - direction
)
(:init
(posaux_pointing satellite0 Star0)
(posaux_pointing satellite0 GroundStation1)
(posaux_pointing satellite0 Star13)
(posaux_pointing satellite0 Star8)
(posaux_pointing satellite0 Star7)
(posaux_pointing satellite0 Star12)
(posaux_pointing satellite0 Star10)
(posaux_pointing satellite0 GroundStation2)
(posaux_pointing satellite0 GroundStation6)
(posaux_pointing satellite0 GroundStation9)
(posaux_pointing satellite0 Star4)
(posaux_pointing satellite0 GroundStation15)
(posaux_pointing satellite0 Star3)
(posaux_pointing satellite0 GroundStation17)
(posaux_pointing satellite0 Star14)
(posaux_pointing satellite0 Star11)
(posaux_pointing satellite0 Star5)
(posaux_pointing satellite0 Star16)
(posaux_pointing satellite0 Phenomenon18)
(posaux_pointing satellite0 Phenomenon19)
(posaux_pointing satellite0 Planet20)
(posaux_pointing satellite0 Star21)
(posaux_pointing satellite0 Phenomenon22)
(posaux_pointing satellite0 Planet23)
(posaux_pointing satellite0 Phenomenon24)
(posaux_pointing satellite0 Star25)
(posaux_pointing satellite0 Planet26)
(posaux_pointing satellite0 Star27)
(posaux_pointing satellite0 Phenomenon28)
(posaux_pointing satellite0 Star29)
(posaux_pointing satellite0 Phenomenon30)
(posaux_pointing satellite0 Phenomenon31)
(posaux_pointing satellite0 Phenomenon32)
(posaux_pointing satellite0 Star33)
(posaux_pointing satellite0 Planet34)
(posaux_pointing satellite0 Star35)
(posaux_pointing satellite0 Planet36)
(posaux_pointing satellite0 Phenomenon37)
(posaux_pointing satellite0 Planet38)
(posaux_pointing satellite0 Planet39)
(posaux_pointing satellite0 Phenomenon40)
(posaux_pointing satellite0 Planet41)
(posaux_pointing satellite0 Phenomenon42)
(posaux_pointing satellite0 Star43)
(posaux_pointing satellite0 Phenomenon44)
(posaux_pointing satellite0 Phenomenon45)
(posaux_pointing satellite1 Star0)
(posaux_pointing satellite1 GroundStation1)
(posaux_pointing satellite1 Star13)
(posaux_pointing satellite1 Star8)
(posaux_pointing satellite1 Star7)
(posaux_pointing satellite1 Star12)
(posaux_pointing satellite1 Star10)
(posaux_pointing satellite1 GroundStation2)
(posaux_pointing satellite1 GroundStation6)
(posaux_pointing satellite1 GroundStation9)
(posaux_pointing satellite1 Star4)
(posaux_pointing satellite1 GroundStation15)
(posaux_pointing satellite1 Star3)
(posaux_pointing satellite1 GroundStation17)
(posaux_pointing satellite1 Star14)
(posaux_pointing satellite1 Star11)
(posaux_pointing satellite1 Star5)
(posaux_pointing satellite1 Star16)
(posaux_pointing satellite1 Phenomenon18)
(posaux_pointing satellite1 Phenomenon19)
(posaux_pointing satellite1 Planet20)
(posaux_pointing satellite1 Star21)
(posaux_pointing satellite1 Phenomenon22)
(posaux_pointing satellite1 Planet23)
(posaux_pointing satellite1 Phenomenon24)
(posaux_pointing satellite1 Star25)
(posaux_pointing satellite1 Planet26)
(posaux_pointing satellite1 Star27)
(posaux_pointing satellite1 Phenomenon28)
(posaux_pointing satellite1 Star29)
(posaux_pointing satellite1 Phenomenon30)
(posaux_pointing satellite1 Phenomenon31)
(posaux_pointing satellite1 Phenomenon32)
(posaux_pointing satellite1 Star33)
(posaux_pointing satellite1 Planet34)
(posaux_pointing satellite1 Star35)
(posaux_pointing satellite1 Planet36)
(posaux_pointing satellite1 Phenomenon37)
(posaux_pointing satellite1 Planet38)
(posaux_pointing satellite1 Planet39)
(posaux_pointing satellite1 Phenomenon40)
(posaux_pointing satellite1 Planet41)
(posaux_pointing satellite1 Phenomenon42)
(posaux_pointing satellite1 Star43)
(posaux_pointing satellite1 Phenomenon44)
(posaux_pointing satellite1 Phenomenon45)
(posaux_pointing satellite2 Star0)
(posaux_pointing satellite2 GroundStation1)
(posaux_pointing satellite2 Star13)
(posaux_pointing satellite2 Star8)
(posaux_pointing satellite2 Star7)
(posaux_pointing satellite2 Star12)
(posaux_pointing satellite2 Star10)
(posaux_pointing satellite2 GroundStation2)
(posaux_pointing satellite2 GroundStation6)
(posaux_pointing satellite2 GroundStation9)
(posaux_pointing satellite2 Star4)
(posaux_pointing satellite2 GroundStation15)
(posaux_pointing satellite2 Star3)
(posaux_pointing satellite2 GroundStation17)
(posaux_pointing satellite2 Star14)
(posaux_pointing satellite2 Star11)
(posaux_pointing satellite2 Star5)
(posaux_pointing satellite2 Star16)
(posaux_pointing satellite2 Phenomenon18)
(posaux_pointing satellite2 Phenomenon19)
(posaux_pointing satellite2 Planet20)
(posaux_pointing satellite2 Star21)
(posaux_pointing satellite2 Phenomenon22)
(posaux_pointing satellite2 Planet23)
(posaux_pointing satellite2 Phenomenon24)
(posaux_pointing satellite2 Star25)
(posaux_pointing satellite2 Planet26)
(posaux_pointing satellite2 Star27)
(posaux_pointing satellite2 Phenomenon28)
(posaux_pointing satellite2 Star29)
(posaux_pointing satellite2 Phenomenon30)
(posaux_pointing satellite2 Phenomenon31)
(posaux_pointing satellite2 Phenomenon32)
(posaux_pointing satellite2 Star33)
(posaux_pointing satellite2 Planet34)
(posaux_pointing satellite2 Star35)
(posaux_pointing satellite2 Planet36)
(posaux_pointing satellite2 Phenomenon37)
(posaux_pointing satellite2 Planet38)
(posaux_pointing satellite2 Planet39)
(posaux_pointing satellite2 Phenomenon40)
(posaux_pointing satellite2 Planet41)
(posaux_pointing satellite2 Phenomenon42)
(posaux_pointing satellite2 Star43)
(posaux_pointing satellite2 Phenomenon44)
(posaux_pointing satellite2 Phenomenon45)
(posaux_pointing satellite3 Star0)
(posaux_pointing satellite3 GroundStation1)
(posaux_pointing satellite3 Star13)
(posaux_pointing satellite3 Star8)
(posaux_pointing satellite3 Star7)
(posaux_pointing satellite3 Star12)
(posaux_pointing satellite3 Star10)
(posaux_pointing satellite3 GroundStation2)
(posaux_pointing satellite3 GroundStation6)
(posaux_pointing satellite3 GroundStation9)
(posaux_pointing satellite3 Star4)
(posaux_pointing satellite3 GroundStation15)
(posaux_pointing satellite3 Star3)
(posaux_pointing satellite3 GroundStation17)
(posaux_pointing satellite3 Star14)
(posaux_pointing satellite3 Star11)
(posaux_pointing satellite3 Star5)
(posaux_pointing satellite3 Star16)
(posaux_pointing satellite3 Phenomenon18)
(posaux_pointing satellite3 Phenomenon19)
(posaux_pointing satellite3 Planet20)
(posaux_pointing satellite3 Star21)
(posaux_pointing satellite3 Phenomenon22)
(posaux_pointing satellite3 Planet23)
(posaux_pointing satellite3 Phenomenon24)
(posaux_pointing satellite3 Star25)
(posaux_pointing satellite3 Planet26)
(posaux_pointing satellite3 Star27)
(posaux_pointing satellite3 Phenomenon28)
(posaux_pointing satellite3 Star29)
(posaux_pointing satellite3 Phenomenon30)
(posaux_pointing satellite3 Phenomenon31)
(posaux_pointing satellite3 Phenomenon32)
(posaux_pointing satellite3 Star33)
(posaux_pointing satellite3 Planet34)
(posaux_pointing satellite3 Star35)
(posaux_pointing satellite3 Planet36)
(posaux_pointing satellite3 Phenomenon37)
(posaux_pointing satellite3 Planet38)
(posaux_pointing satellite3 Planet39)
(posaux_pointing satellite3 Phenomenon40)
(posaux_pointing satellite3 Planet41)
(posaux_pointing satellite3 Phenomenon42)
(posaux_pointing satellite3 Star43)
(posaux_pointing satellite3 Phenomenon44)
(posaux_pointing satellite3 Phenomenon45)
(posaux_pointing satellite4 Star0)
(posaux_pointing satellite4 GroundStation1)
(posaux_pointing satellite4 Star13)
(posaux_pointing satellite4 Star8)
(posaux_pointing satellite4 Star7)
(posaux_pointing satellite4 Star12)
(posaux_pointing satellite4 Star10)
(posaux_pointing satellite4 GroundStation2)
(posaux_pointing satellite4 GroundStation6)
(posaux_pointing satellite4 GroundStation9)
(posaux_pointing satellite4 Star4)
(posaux_pointing satellite4 GroundStation15)
(posaux_pointing satellite4 Star3)
(posaux_pointing satellite4 GroundStation17)
(posaux_pointing satellite4 Star14)
(posaux_pointing satellite4 Star11)
(posaux_pointing satellite4 Star5)
(posaux_pointing satellite4 Star16)
(posaux_pointing satellite4 Phenomenon18)
(posaux_pointing satellite4 Phenomenon19)
(posaux_pointing satellite4 Planet20)
(posaux_pointing satellite4 Star21)
(posaux_pointing satellite4 Phenomenon22)
(posaux_pointing satellite4 Planet23)
(posaux_pointing satellite4 Phenomenon24)
(posaux_pointing satellite4 Star25)
(posaux_pointing satellite4 Planet26)
(posaux_pointing satellite4 Star27)
(posaux_pointing satellite4 Phenomenon28)
(posaux_pointing satellite4 Star29)
(posaux_pointing satellite4 Phenomenon30)
(posaux_pointing satellite4 Phenomenon31)
(posaux_pointing satellite4 Phenomenon32)
(posaux_pointing satellite4 Star33)
(posaux_pointing satellite4 Planet34)
(posaux_pointing satellite4 Star35)
(posaux_pointing satellite4 Planet36)
(posaux_pointing satellite4 Phenomenon37)
(posaux_pointing satellite4 Planet38)
(posaux_pointing satellite4 Planet39)
(posaux_pointing satellite4 Phenomenon40)
(posaux_pointing satellite4 Planet41)
(posaux_pointing satellite4 Phenomenon42)
(posaux_pointing satellite4 Star43)
(posaux_pointing satellite4 Phenomenon44)
(posaux_pointing satellite4 Phenomenon45)
(negaux_pointing satellite0 Star0)
(negaux_pointing satellite0 GroundStation1)
(negaux_pointing satellite0 Star13)
(negaux_pointing satellite0 Star8)
(negaux_pointing satellite0 Star7)
(negaux_pointing satellite0 Star12)
(negaux_pointing satellite0 Star10)
(negaux_pointing satellite0 GroundStation2)
(negaux_pointing satellite0 GroundStation6)
(negaux_pointing satellite0 GroundStation9)
(negaux_pointing satellite0 Star4)
(negaux_pointing satellite0 GroundStation15)
(negaux_pointing satellite0 Star3)
(negaux_pointing satellite0 GroundStation17)
(negaux_pointing satellite0 Star14)
(negaux_pointing satellite0 Star11)
(negaux_pointing satellite0 Star5)
(negaux_pointing satellite0 Star16)
(negaux_pointing satellite0 Phenomenon18)
(negaux_pointing satellite0 Phenomenon19)
(negaux_pointing satellite0 Planet20)
(negaux_pointing satellite0 Star21)
(negaux_pointing satellite0 Phenomenon22)
(negaux_pointing satellite0 Planet23)
(negaux_pointing satellite0 Phenomenon24)
(negaux_pointing satellite0 Star25)
(negaux_pointing satellite0 Planet26)
(negaux_pointing satellite0 Star27)
(negaux_pointing satellite0 Phenomenon28)
(negaux_pointing satellite0 Star29)
(negaux_pointing satellite0 Phenomenon30)
(negaux_pointing satellite0 Phenomenon31)
(negaux_pointing satellite0 Phenomenon32)
(negaux_pointing satellite0 Star33)
(negaux_pointing satellite0 Planet34)
(negaux_pointing satellite0 Star35)
(negaux_pointing satellite0 Planet36)
(negaux_pointing satellite0 Phenomenon37)
(negaux_pointing satellite0 Planet38)
(negaux_pointing satellite0 Planet39)
(negaux_pointing satellite0 Phenomenon40)
(negaux_pointing satellite0 Planet41)
(negaux_pointing satellite0 Phenomenon42)
(negaux_pointing satellite0 Star43)
(negaux_pointing satellite0 Phenomenon44)
(negaux_pointing satellite0 Phenomenon45)
(negaux_pointing satellite1 Star0)
(negaux_pointing satellite1 GroundStation1)
(negaux_pointing satellite1 Star13)
(negaux_pointing satellite1 Star8)
(negaux_pointing satellite1 Star7)
(negaux_pointing satellite1 Star12)
(negaux_pointing satellite1 Star10)
(negaux_pointing satellite1 GroundStation2)
(negaux_pointing satellite1 GroundStation6)
(negaux_pointing satellite1 GroundStation9)
(negaux_pointing satellite1 Star4)
(negaux_pointing satellite1 GroundStation15)
(negaux_pointing satellite1 Star3)
(negaux_pointing satellite1 GroundStation17)
(negaux_pointing satellite1 Star14)
(negaux_pointing satellite1 Star11)
(negaux_pointing satellite1 Star5)
(negaux_pointing satellite1 Star16)
(negaux_pointing satellite1 Phenomenon18)
(negaux_pointing satellite1 Phenomenon19)
(negaux_pointing satellite1 Planet20)
(negaux_pointing satellite1 Star21)
(negaux_pointing satellite1 Phenomenon22)
(negaux_pointing satellite1 Planet23)
(negaux_pointing satellite1 Phenomenon24)
(negaux_pointing satellite1 Star25)
(negaux_pointing satellite1 Planet26)
(negaux_pointing satellite1 Star27)
(negaux_pointing satellite1 Phenomenon28)
(negaux_pointing satellite1 Star29)
(negaux_pointing satellite1 Phenomenon30)
(negaux_pointing satellite1 Phenomenon31)
(negaux_pointing satellite1 Phenomenon32)
(negaux_pointing satellite1 Star33)
(negaux_pointing satellite1 Planet34)
(negaux_pointing satellite1 Star35)
(negaux_pointing satellite1 Planet36)
(negaux_pointing satellite1 Phenomenon37)
(negaux_pointing satellite1 Planet38)
(negaux_pointing satellite1 Planet39)
(negaux_pointing satellite1 Phenomenon40)
(negaux_pointing satellite1 Planet41)
(negaux_pointing satellite1 Phenomenon42)
(negaux_pointing satellite1 Star43)
(negaux_pointing satellite1 Phenomenon44)
(negaux_pointing satellite1 Phenomenon45)
(negaux_pointing satellite2 Star0)
(negaux_pointing satellite2 GroundStation1)
(negaux_pointing satellite2 Star13)
(negaux_pointing satellite2 Star8)
(negaux_pointing satellite2 Star7)
(negaux_pointing satellite2 Star12)
(negaux_pointing satellite2 Star10)
(negaux_pointing satellite2 GroundStation2)
(negaux_pointing satellite2 GroundStation6)
(negaux_pointing satellite2 GroundStation9)
(negaux_pointing satellite2 Star4)
(negaux_pointing satellite2 GroundStation15)
(negaux_pointing satellite2 Star3)
(negaux_pointing satellite2 GroundStation17)
(negaux_pointing satellite2 Star14)
(negaux_pointing satellite2 Star11)
(negaux_pointing satellite2 Star5)
(negaux_pointing satellite2 Star16)
(negaux_pointing satellite2 Phenomenon18)
(negaux_pointing satellite2 Phenomenon19)
(negaux_pointing satellite2 Planet20)
(negaux_pointing satellite2 Star21)
(negaux_pointing satellite2 Phenomenon22)
(negaux_pointing satellite2 Planet23)
(negaux_pointing satellite2 Phenomenon24)
(negaux_pointing satellite2 Star25)
(negaux_pointing satellite2 Planet26)
(negaux_pointing satellite2 Star27)
(negaux_pointing satellite2 Phenomenon28)
(negaux_pointing satellite2 Star29)
(negaux_pointing satellite2 Phenomenon30)
(negaux_pointing satellite2 Phenomenon31)
(negaux_pointing satellite2 Phenomenon32)
(negaux_pointing satellite2 Star33)
(negaux_pointing satellite2 Planet34)
(negaux_pointing satellite2 Star35)
(negaux_pointing satellite2 Planet36)
(negaux_pointing satellite2 Phenomenon37)
(negaux_pointing satellite2 Planet38)
(negaux_pointing satellite2 Planet39)
(negaux_pointing satellite2 Phenomenon40)
(negaux_pointing satellite2 Planet41)
(negaux_pointing satellite2 Phenomenon42)
(negaux_pointing satellite2 Star43)
(negaux_pointing satellite2 Phenomenon44)
(negaux_pointing satellite2 Phenomenon45)
(negaux_pointing satellite3 Star0)
(negaux_pointing satellite3 GroundStation1)
(negaux_pointing satellite3 Star13)
(negaux_pointing satellite3 Star8)
(negaux_pointing satellite3 Star7)
(negaux_pointing satellite3 Star12)
(negaux_pointing satellite3 Star10)
(negaux_pointing satellite3 GroundStation2)
(negaux_pointing satellite3 GroundStation6)
(negaux_pointing satellite3 GroundStation9)
(negaux_pointing satellite3 Star4)
(negaux_pointing satellite3 GroundStation15)
(negaux_pointing satellite3 Star3)
(negaux_pointing satellite3 GroundStation17)
(negaux_pointing satellite3 Star14)
(negaux_pointing satellite3 Star11)
(negaux_pointing satellite3 Star5)
(negaux_pointing satellite3 Star16)
(negaux_pointing satellite3 Phenomenon18)
(negaux_pointing satellite3 Phenomenon19)
(negaux_pointing satellite3 Planet20)
(negaux_pointing satellite3 Star21)
(negaux_pointing satellite3 Phenomenon22)
(negaux_pointing satellite3 Planet23)
(negaux_pointing satellite3 Phenomenon24)
(negaux_pointing satellite3 Star25)
(negaux_pointing satellite3 Planet26)
(negaux_pointing satellite3 Star27)
(negaux_pointing satellite3 Phenomenon28)
(negaux_pointing satellite3 Star29)
(negaux_pointing satellite3 Phenomenon30)
(negaux_pointing satellite3 Phenomenon31)
(negaux_pointing satellite3 Phenomenon32)
(negaux_pointing satellite3 Star33)
(negaux_pointing satellite3 Planet34)
(negaux_pointing satellite3 Star35)
(negaux_pointing satellite3 Planet36)
(negaux_pointing satellite3 Phenomenon37)
(negaux_pointing satellite3 Planet38)
(negaux_pointing satellite3 Planet39)
(negaux_pointing satellite3 Phenomenon40)
(negaux_pointing satellite3 Planet41)
(negaux_pointing satellite3 Phenomenon42)
(negaux_pointing satellite3 Star43)
(negaux_pointing satellite3 Phenomenon44)
(negaux_pointing satellite3 Phenomenon45)
(negaux_pointing satellite4 Star0)
(negaux_pointing satellite4 GroundStation1)
(negaux_pointing satellite4 Star13)
(negaux_pointing satellite4 Star8)
(negaux_pointing satellite4 Star7)
(negaux_pointing satellite4 Star12)
(negaux_pointing satellite4 Star10)
(negaux_pointing satellite4 GroundStation2)
(negaux_pointing satellite4 GroundStation6)
(negaux_pointing satellite4 GroundStation9)
(negaux_pointing satellite4 Star4)
(negaux_pointing satellite4 GroundStation15)
(negaux_pointing satellite4 Star3)
(negaux_pointing satellite4 GroundStation17)
(negaux_pointing satellite4 Star14)
(negaux_pointing satellite4 Star11)
(negaux_pointing satellite4 Star5)
(negaux_pointing satellite4 Star16)
(negaux_pointing satellite4 Phenomenon18)
(negaux_pointing satellite4 Phenomenon19)
(negaux_pointing satellite4 Planet20)
(negaux_pointing satellite4 Star21)
(negaux_pointing satellite4 Phenomenon22)
(negaux_pointing satellite4 Planet23)
(negaux_pointing satellite4 Phenomenon24)
(negaux_pointing satellite4 Star25)
(negaux_pointing satellite4 Planet26)
(negaux_pointing satellite4 Star27)
(negaux_pointing satellite4 Phenomenon28)
(negaux_pointing satellite4 Star29)
(negaux_pointing satellite4 Phenomenon30)
(negaux_pointing satellite4 Phenomenon31)
(negaux_pointing satellite4 Phenomenon32)
(negaux_pointing satellite4 Star33)
(negaux_pointing satellite4 Planet34)
(negaux_pointing satellite4 Star35)
(negaux_pointing satellite4 Planet36)
(negaux_pointing satellite4 Phenomenon37)
(negaux_pointing satellite4 Planet38)
(negaux_pointing satellite4 Planet39)
(negaux_pointing satellite4 Phenomenon40)
(negaux_pointing satellite4 Planet41)
(negaux_pointing satellite4 Phenomenon42)
(negaux_pointing satellite4 Star43)
(negaux_pointing satellite4 Phenomenon44)
(negaux_pointing satellite4 Phenomenon45)
	(supports instrument0 thermograph12)
	(supports instrument0 spectrograph2)
	(calibration_target instrument0 GroundStation1)
	(calibration_target instrument0 Star0)
	(calibration_target instrument0 GroundStation6)
	(calibration_target instrument0 GroundStation2)
	(calibration_target instrument0 Star14)
	(on_board instrument0 satellite0)
	(power_avail satellite0)
	(pointing satellite0 GroundStation2)
	(supports instrument1 thermograph6)
	(supports instrument1 thermograph12)
	(calibration_target instrument1 GroundStation9)
	(calibration_target instrument1 GroundStation17)
	(calibration_target instrument1 GroundStation15)
	(calibration_target instrument1 Star8)
	(supports instrument2 thermograph5)
	(supports instrument2 image11)
	(supports instrument2 thermograph12)
	(calibration_target instrument2 Star0)
	(calibration_target instrument2 Star14)
	(calibration_target instrument2 GroundStation15)
	(calibration_target instrument2 Star11)
	(calibration_target instrument2 Star5)
	(calibration_target instrument2 Star3)
	(supports instrument3 spectrograph9)
	(calibration_target instrument3 Star16)
	(calibration_target instrument3 GroundStation6)
	(calibration_target instrument3 Star12)
	(calibration_target instrument3 Star3)
	(calibration_target instrument3 Star7)
	(supports instrument4 thermograph7)
	(supports instrument4 image0)
	(calibration_target instrument4 GroundStation2)
	(calibration_target instrument4 Star5)
	(supports instrument5 image4)
	(calibration_target instrument5 GroundStation9)
	(calibration_target instrument5 Star14)
	(calibration_target instrument5 GroundStation17)
	(calibration_target instrument5 Star3)
	(calibration_target instrument5 Star8)
	(calibration_target instrument5 GroundStation15)
	(supports instrument6 spectrograph2)
	(supports instrument6 thermograph10)
	(supports instrument6 image0)
	(calibration_target instrument6 Star7)
	(calibration_target instrument6 Star0)
	(on_board instrument1 satellite1)
	(on_board instrument2 satellite1)
	(on_board instrument3 satellite1)
	(on_board instrument4 satellite1)
	(on_board instrument5 satellite1)
	(on_board instrument6 satellite1)
	(power_avail satellite1)
	(pointing satellite1 Star7)
	(supports instrument7 spectrograph9)
	(calibration_target instrument7 Star3)
	(calibration_target instrument7 Star13)
	(calibration_target instrument7 Star7)
	(calibration_target instrument7 Star12)
	(supports instrument8 spectrograph9)
	(calibration_target instrument8 GroundStation2)
	(calibration_target instrument8 Star7)
	(calibration_target instrument8 Star4)
	(calibration_target instrument8 Star0)
	(supports instrument9 spectrograph13)
	(supports instrument9 thermograph7)
	(calibration_target instrument9 Star8)
	(calibration_target instrument9 GroundStation17)
	(calibration_target instrument9 Star4)
	(calibration_target instrument9 Star13)
	(calibration_target instrument9 GroundStation1)
	(calibration_target instrument9 GroundStation6)
	(supports instrument10 image8)
	(supports instrument10 thermograph5)
	(supports instrument10 spectrograph9)
	(calibration_target instrument10 GroundStation9)
	(calibration_target instrument10 Star3)
	(supports instrument11 image0)
	(supports instrument11 thermograph5)
	(calibration_target instrument11 GroundStation1)
	(calibration_target instrument11 Star8)
	(calibration_target instrument11 Star11)
	(calibration_target instrument11 Star13)
	(calibration_target instrument11 Star0)
	(supports instrument12 spectrograph1)
	(calibration_target instrument12 Star0)
	(calibration_target instrument12 Star11)
	(supports instrument13 thermograph7)
	(calibration_target instrument13 GroundStation6)
	(calibration_target instrument13 Star12)
	(calibration_target instrument13 Star8)
	(supports instrument14 image11)
	(calibration_target instrument14 Star10)
	(supports instrument15 thermograph7)
	(supports instrument15 thermograph6)
	(calibration_target instrument15 Star4)
	(calibration_target instrument15 GroundStation15)
	(calibration_target instrument15 GroundStation9)
	(on_board instrument7 satellite2)
	(on_board instrument8 satellite2)
	(on_board instrument9 satellite2)
	(on_board instrument10 satellite2)
	(on_board instrument11 satellite2)
	(on_board instrument12 satellite2)
	(on_board instrument13 satellite2)
	(on_board instrument14 satellite2)
	(on_board instrument15 satellite2)
	(power_avail satellite2)
	(pointing satellite2 Star33)
	(supports instrument16 thermograph5)
	(supports instrument16 thermograph6)
	(supports instrument16 spectrograph13)
	(calibration_target instrument16 Star12)
	(calibration_target instrument16 GroundStation2)
	(calibration_target instrument16 GroundStation6)
	(calibration_target instrument16 Star14)
	(calibration_target instrument16 Star13)
	(calibration_target instrument16 Star4)
	(supports instrument17 image11)
	(calibration_target instrument17 GroundStation15)
	(calibration_target instrument17 GroundStation2)
	(supports instrument18 thermograph10)
	(calibration_target instrument18 Star3)
	(calibration_target instrument18 Star7)
	(calibration_target instrument18 Star13)
	(calibration_target instrument18 Star14)
	(supports instrument19 spectrograph2)
	(calibration_target instrument19 Star7)
	(calibration_target instrument19 Star10)
	(calibration_target instrument19 GroundStation1)
	(calibration_target instrument19 Star13)
	(calibration_target instrument19 Star12)
	(supports instrument20 spectrograph13)
	(supports instrument20 spectrograph9)
	(calibration_target instrument20 GroundStation15)
	(calibration_target instrument20 GroundStation2)
	(supports instrument21 spectrograph2)
	(supports instrument21 infrared3)
	(supports instrument21 spectrograph9)
	(calibration_target instrument21 GroundStation1)
	(calibration_target instrument21 GroundStation15)
	(calibration_target instrument21 Star12)
	(on_board instrument16 satellite3)
	(on_board instrument17 satellite3)
	(on_board instrument18 satellite3)
	(on_board instrument19 satellite3)
	(on_board instrument20 satellite3)
	(on_board instrument21 satellite3)
	(power_avail satellite3)
	(pointing satellite3 Star16)
	(supports instrument22 image11)
	(calibration_target instrument22 Star5)
	(calibration_target instrument22 Star13)
	(supports instrument23 image8)
	(supports instrument23 spectrograph1)
	(supports instrument23 thermograph5)
	(calibration_target instrument23 GroundStation17)
	(calibration_target instrument23 Star8)
	(calibration_target instrument23 Star4)
	(supports instrument24 image4)
	(supports instrument24 spectrograph13)
	(supports instrument24 thermograph7)
	(calibration_target instrument24 Star14)
	(calibration_target instrument24 Star4)
	(calibration_target instrument24 Star8)
	(calibration_target instrument24 Star11)
	(supports instrument25 infrared3)
	(supports instrument25 spectrograph13)
	(calibration_target instrument25 Star14)
	(calibration_target instrument25 Star5)
	(calibration_target instrument25 Star10)
	(calibration_target instrument25 Star11)
	(supports instrument26 image11)
	(supports instrument26 spectrograph1)
	(supports instrument26 spectrograph13)
	(calibration_target instrument26 Star7)
	(calibration_target instrument26 Star4)
	(calibration_target instrument26 Star8)
	(calibration_target instrument26 Star11)
	(supports instrument27 thermograph6)
	(calibration_target instrument27 Star10)
	(calibration_target instrument27 Star4)
	(calibration_target instrument27 Star12)
	(calibration_target instrument27 GroundStation9)
	(calibration_target instrument27 GroundStation2)
	(supports instrument28 spectrograph1)
	(calibration_target instrument28 GroundStation17)
	(calibration_target instrument28 GroundStation6)
	(calibration_target instrument28 Star14)
	(calibration_target instrument28 Star4)
	(calibration_target instrument28 GroundStation2)
	(supports instrument29 thermograph12)
	(supports instrument29 image4)
	(supports instrument29 thermograph6)
	(calibration_target instrument29 Star4)
	(calibration_target instrument29 Star16)
	(calibration_target instrument29 GroundStation9)
	(calibration_target instrument29 Star11)
	(calibration_target instrument29 GroundStation17)
	(calibration_target instrument29 GroundStation6)
	(supports instrument30 image11)
	(supports instrument30 thermograph12)
	(calibration_target instrument30 Star3)
	(calibration_target instrument30 GroundStation15)
	(supports instrument31 image0)
	(supports instrument31 infrared3)
	(supports instrument31 image11)
	(calibration_target instrument31 Star16)
	(supports instrument32 thermograph7)
	(calibration_target instrument32 Star16)
	(calibration_target instrument32 Star5)
	(calibration_target instrument32 Star11)
	(calibration_target instrument32 Star14)
	(calibration_target instrument32 GroundStation17)
	(on_board instrument22 satellite4)
	(on_board instrument23 satellite4)
	(on_board instrument24 satellite4)
	(on_board instrument25 satellite4)
	(on_board instrument26 satellite4)
	(on_board instrument27 satellite4)
	(on_board instrument28 satellite4)
	(on_board instrument29 satellite4)
	(on_board instrument30 satellite4)
	(on_board instrument31 satellite4)
	(on_board instrument32 satellite4)
	(power_avail satellite4)
	(pointing satellite4 Planet26)
)
(:goal (and
	(pointing satellite1 Planet36)
	(have_image Phenomenon18 thermograph12)
	(have_image Phenomenon18 image8)
	(have_image Phenomenon18 infrared3)
	(have_image Phenomenon18 image4)
	(have_image Phenomenon19 image11)
	(have_image Phenomenon19 spectrograph1)
	(have_image Phenomenon19 spectrograph9)
	(have_image Planet20 image4)
	(have_image Planet20 thermograph5)
	(have_image Planet20 thermograph6)
	(have_image Planet20 spectrograph13)
	(have_image Star21 thermograph6)
	(have_image Star21 thermograph12)
	(have_image Star21 image11)
	(have_image Phenomenon22 spectrograph9)
	(have_image Phenomenon22 thermograph5)
	(have_image Phenomenon22 image11)
	(have_image Phenomenon22 thermograph6)
	(have_image Phenomenon24 spectrograph1)
	(have_image Phenomenon24 thermograph7)
	(have_image Star25 spectrograph9)
	(have_image Star25 spectrograph1)
	(have_image Planet26 spectrograph1)
	(have_image Planet26 thermograph5)
	(have_image Planet26 infrared3)
	(have_image Planet26 image4)
	(have_image Star27 image11)
	(have_image Star27 spectrograph2)
	(have_image Phenomenon28 thermograph10)
	(have_image Phenomenon28 spectrograph13)
	(have_image Star29 image11)
	(have_image Star29 thermograph12)
	(have_image Star29 spectrograph1)
	(have_image Phenomenon30 thermograph12)
	(have_image Phenomenon31 spectrograph1)
	(have_image Star33 image0)
	(have_image Planet34 spectrograph13)
	(have_image Planet34 thermograph12)
	(have_image Planet34 image8)
	(have_image Star35 spectrograph2)
	(have_image Star35 thermograph6)
	(have_image Star35 thermograph5)
	(have_image Planet36 thermograph6)
	(have_image Planet36 thermograph10)
	(have_image Planet36 thermograph5)
	(have_image Planet36 thermograph7)
	(have_image Phenomenon37 image8)
	(have_image Phenomenon37 thermograph5)
	(have_image Phenomenon37 spectrograph9)
	(have_image Planet38 infrared3)
	(have_image Planet38 spectrograph9)
	(have_image Planet38 thermograph7)
	(have_image Planet39 image0)
	(have_image Phenomenon40 thermograph7)
	(have_image Phenomenon40 image4)
	(have_image Phenomenon40 image0)
	(have_image Phenomenon40 thermograph12)
	(have_image Planet41 image11)
	(have_image Planet41 image0)
	(have_image Phenomenon42 thermograph5)
	(have_image Star43 spectrograph9)
	(have_image Star43 thermograph12)
	(have_image Phenomenon44 image0)
	(have_image Phenomenon44 image4)
	(have_image Phenomenon44 thermograph10)
	(have_image Phenomenon44 thermograph12)
	(have_image Phenomenon45 image0)
	(have_image Phenomenon45 thermograph5)
	(have_image Phenomenon45 image11)
))
(:metric minimize (total-time))

)
