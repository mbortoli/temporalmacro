(define (problem task)
(:domain tdomain)
(:objects
    start - starting
    l1 l3 l5 - getlocation
    l2 l4 l6 - deliverlocation
    r1 r2 - robot
)

(:init
   (at r1 start)
   (free l1)
   (free l2)
   (free l3)
   (free l4)
   (at r2 l5)
   (free l6)
   (empty r1)
(negaux_at_r_l)
(posaux_empty_r)
(posaux_holding_r)
(negaux_free_l)
(posaux_free_l)
(negaux_empty_r)
(negaux_holding_r)


)

(:goal (and
   (level r1)

)
)

)