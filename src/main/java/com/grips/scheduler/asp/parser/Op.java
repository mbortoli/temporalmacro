/*
 * Copyright (c) 2010-2012 by Damien Pellier <Damien.Pellier@imag.fr>.
 *
 * This file is part of PDDL4J library.
 *
 * PDDL4J is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PDDL4J is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PDDL4J.  If not, see <http://www.gnu.org/licenses/>
 */

package com.grips.scheduler.asp.parser;

import com.grips.scheduler.asp.PreProcessing.Pair;
import com.grips.scheduler.asp.exceptions.FatalException;
import com.grips.scheduler.asp.util.IntExp;

import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

/**
 * This class implements a planning operator parsed.
 * <p>
 * Modifications:
 * </p>
 * <ul>
 * <li> Add method normalize(int i) - 11.12.2012.</li>
 * <li> Add constructor of copy - 11.12.2012.</li>
 * </ul>
 *
 * @author D. Pellier
 * @version 1.1 - 28.01.2010
 */
public class Op implements Serializable {

    private Set<Exp> neg_pred_atstart;
    private Set<Exp> pos_pred_atstart;

    private Set<Exp> neg_pred_atend;
    private Set<Exp> pos_pred_atend;

    private Set<Exp> neg_pred_overall;
    private Set<Exp> pos_pred_overall;

    private Set<Exp> neg_eff_atstart;
    private Set<Exp> pos_eff_atstart;

    private Set<Exp> neg_eff_atend;
    private Set<Exp> pos_eff_atend;

    private Set<Exp> neg_eff_overall;
    private Set<Exp> pos_eff_overall;

    private List<Pair<String,String>> sparameters;



    /**
     * The serial id of the class.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The name of the operator.
     */
    private Symbol name;

    /**
     * The list of parameters of the operators.
     */
    private List<TypedSymbol> parameters;

    /**
     * The goal description that represents the preconditions of the operator.
     */
    private Exp preconditions;

    /**
     * The goal description that represents the effects of the operator.
     */
    private Exp effects;

    /**
     * The goal description that represents the constraints duration of temporal operator.
     */
    private Exp duration;

    /**
     * Create a new operator.
     */
    private Op() {
        super();
        this.name = null;
        this.parameters = null;
        this.preconditions = null;
        this.effects = null;
        this.duration = null;
    }


    /**
     * Create a new operator from another.
     *
     * @param other the other operator.
     */
    public Op(final Op other) {
        if (other == null) {
            throw new NullPointerException();
        }
        this.name = new Symbol(other.getName());
        this.parameters = new LinkedList<>();
        this.parameters.addAll(other.getParameters().stream().map(TypedSymbol::new).collect(Collectors.toList()));
        this.preconditions = new Exp(other.getPreconditions());
        this.effects = new Exp(other.getEffects());
        if (this.duration != null) {
            this.duration = new Exp(other.getDuration());
        }
    }

    /**
     * Creates operator with a specified name, list of parameters, preconditions and effects.
     *
     * @param name       The name of the operator.
     * @param parameters The list of parameters of the operator.
     * @param preconds   The goal description that represents the preconditions of the operator.
     * @param effects    The goal description that represents the effects of the operator.
     */
    public Op(final Symbol name, final List<TypedSymbol> parameters, final Exp preconds, final Exp effects) {
        this(name, parameters, preconds, effects, null);
    }

    /**
     * Creates operator with a specified name, list of parameters, preconditions and effects.
     *
     * @param name          The name of the operator.
     * @param parameters    The list of parameters of the operator.
     * @param preconditions The goal description that represents the preconditions of the operator.
     * @param effects       The goal description that represents the effects of the operator.
     * @param duration      The goal description that represents the duration constraints of the
     *                      operator.
     * @throws NullPointerException if the specified name, parameters, preconditions or effects are
     *                              null.
     */
    public Op(final Symbol name, final List<TypedSymbol> parameters, final Exp preconditions, final Exp effects,
              final Exp duration) {
        this();
        if (name == null || parameters == null || preconditions == null || effects == null) {
            throw new NullPointerException();
        }
        this.name = name;
        this.parameters = parameters;
        this.preconditions = preconditions;
        this.effects = effects;
        this.duration = duration;
    }

    public void replaceParameter (String from, String to,String toType) {

        replaceParameterInSparameters(from,to,toType);

        replaceParameterInExp(pos_pred_atstart,from,to);
        replaceParameterInExp(pos_pred_atend,from,to);
        replaceParameterInExp(pos_pred_overall,from,to);

        replaceParameterInExp(pos_eff_atstart,from,to);
        replaceParameterInExp(pos_eff_atend,from,to);
        replaceParameterInExp(neg_eff_atstart,from,to);
        replaceParameterInExp(neg_eff_atend,from,to);



    }

    public List<Pair<String, String>> getSparameters() {
        return sparameters;
    }

    private void replaceParameterInSparameters(String from, String to, String toType) {
        System.out.println("trying to replace " + from + " with " + to + " of type " + toType);
        for (Pair<String,String> p : sparameters) {
            if (p.first.equals(from)) {
                p.first = to;
                p.second = toType;
            }
        }
        System.out.println("pam at end" + sparameters);
    }

    public void createsparameter() {
        sparameters = new ArrayList<>();
        for (TypedSymbol p: parameters) {
            String [] parse = p.toString().split(" - ");
            sparameters.add(new Pair<>(parse[0].substring(1),parse[1]));
        }
        System.out.println(sparameters);
    }

    private void replaceParameterInExp(Set<Exp> predicates, String from, String to) {
        for (Exp e: predicates) {

            List<Symbol> atom = e.getAtom();
            for (Symbol s: atom) {
                if (s.toString().equals("?"+from)){
                    s.setImage("?"+to);
                }
            }
        }
    }

    public String getParamType(String first) {
        String type = "";
        for (TypedSymbol p: parameters) {
            if (p.toString().contains("?"+first)) {
                System.out.println("p is " + p);
                type = p.toString().substring(first.length() + 4);
            }

        }
        return type;
    }


    public void generateLists () {
        neg_pred_atstart = new HashSet<Exp>();
        pos_pred_atstart = new HashSet<Exp>();
        neg_pred_atend = new HashSet<Exp>();
        pos_pred_atend = new HashSet<Exp>();
        neg_pred_overall = new HashSet<Exp>();
        pos_pred_overall = new HashSet<Exp>();

        neg_eff_atstart = new HashSet<Exp>();
        pos_eff_atstart = new HashSet<Exp>();
        neg_eff_atend = new HashSet<Exp>();
        pos_eff_atend = new HashSet<Exp>();
        neg_eff_overall = new HashSet<Exp>();
        pos_eff_overall = new HashSet<Exp>();

        neg_pred_atstart.clear();
        pos_pred_atstart.clear();
        neg_pred_atend.clear();
        pos_pred_atend.clear();
        neg_pred_overall.clear();
        pos_pred_overall.clear();

        neg_eff_atstart.clear();
        pos_eff_atstart.clear();
        neg_eff_atend.clear();
        pos_eff_atend.clear();
        neg_eff_overall.clear();
        pos_eff_overall.clear();


        switch (preconditions.getConnective()){
            case AND:
                List<Exp> children = preconditions.getChildren();
                Iterator<Exp> it = children.iterator();
                while(it.hasNext()){
                    collect_atom(it.next(), neg_pred_atstart, pos_pred_atstart, neg_pred_atend, pos_pred_atend, neg_pred_overall, pos_pred_overall);
                }
                break;

            default:
                System.out.println(preconditions.getConnective().toString());
                break;
        }
        switch (effects.getConnective()){
            case AND:
                List<Exp> children = effects.getChildren();
                Iterator<Exp> it = children.iterator();
                while(it.hasNext()){
                    collect_atom(it.next(), neg_eff_atstart, pos_eff_atstart, neg_eff_atend, pos_eff_atend, neg_eff_overall, pos_eff_overall);
                }
                break;

            default:
                System.out.println(preconditions.getConnective().toString());
                break;
        }
    }

    private void collect_atom(Exp atom, Set<Exp> neg_atstart, Set<Exp> pos_atstart, Set<Exp> neg_atend, Set<Exp> pos_atend,
                              Set<Exp> neg_overall, Set<Exp> pos_overall) {
        if (atom.getChildren().size() > 1)
            System.out.println("ERROR: temporal atom has more than 1 children ");
        else {
            Exp atom2 = atom.getChildren().get(0);
            switch (atom.getConnective()) {
                case AT_START:
                    switch (atom2.getConnective()) {
                        case NOT:   //negative atom
                            //System.out.println("trying to add neg "+ atom2.getChildren().get(0));
                            neg_atstart.add(atom2.getChildren().get(0));
                            break;
                        case ATOM:    //positive atom
                            //System.out.println("trying to add "+ atom2);
                            pos_atstart.add(atom2);
                            break;
                        default:
                            System.out.println("unexpected atom" + atom2);
                            break;
                    }
                    break;
                case AT_END:
                    switch (atom2.getConnective()) {
                        case NOT:   //negative atom
                            //System.out.println("trying to add neg end "+ atom2.getChildren().get(0));
                            neg_atend.add(atom2.getChildren().get(0));
                            break;
                        case ATOM:    //positive atom
                            //System.out.println("trying to add "+ atom2.getPredicate());
                            pos_atend.add(atom2);
                            break;
                        default:
                            System.out.println("unexpected atom" + atom2);
                            break;
                    }
                    break;
                case OVER_ALL:
                    switch (atom2.getConnective()) {
                        case NOT:   //negative atom
                            //System.out.println("trying to add neg over "+ atom2.getChildren().get(0).getPredicate());
                            neg_overall.add(atom2.getChildren().get(0));
                            break;
                        case ATOM:    //positive atom
                            //System.out.println("trying to add over"+ atom2.getPredicate());
                            pos_overall.add(atom2);
                            break;
                        default:
                            System.out.println("unexpected atom" + atom2);
                            break;
                    }
                    break;
                default:
                    System.out.println("unexpected atom in collect_atom");
                    break;
            }
        }
    }

    public Set<Exp> getNeg_pred_atstart() {
        return neg_pred_atstart;
    }

    public Set<Exp> getPos_pred_atstart() {
        return pos_pred_atstart;
    }

    public Set<Exp> getNeg_pred_atend() {
        return neg_pred_atend;
    }

    public Set<Exp> getPos_pred_atend() {
        return pos_pred_atend;
    }

    public Set<Exp> getNeg_pred_overall() {
        return neg_pred_overall;
    }

    public Set<Exp> getPos_pred_overall() {
        return pos_pred_overall;
    }

    public Set<Exp> getNeg_eff_atstart() {
        return neg_eff_atstart;
    }

    public Set<Exp> getPos_eff_atstart() {
        return pos_eff_atstart;
    }

    public Set<Exp> getNeg_eff_atend() {
        return neg_eff_atend;
    }

    public Set<Exp> getPos_eff_atend() {
        return pos_eff_atend;
    }

    public Set<Exp> getNeg_eff_overall() {
        return neg_eff_overall;
    }

    public Set<Exp> getPos_eff_overall() {
        return pos_eff_overall;
    }

    /**
     * Returns the name of the operator.
     *
     * @return the name of the operator.
     */
    public final Symbol getName() {
        return this.name;
    }

    /**
     * Sets a new name to the operator.
     *
     * @param name the name to set.
     */
    public final void setName(final Symbol name) {
        if (name == null) {
            throw new NullPointerException();
        }
        this.name = name;
    }

    /**
     * Returns the list of parameters of the operator.
     *
     * @return the list of parameters of the operator.
     */
    public final List<TypedSymbol> getParameters() {
        return this.parameters;
    }

    /**
     * Returns the parameter of the operator that has a specified symbol.
     *
     * @param symbol The symbol.
     * @return the parameter of the operator that has a specified symbol or <code>null</code> if the
     *          operator has no such parameter.
     */
    public final TypedSymbol getParameter(final Symbol symbol) {
        final int index = this.parameters.indexOf(symbol);
        return (index == -1) ? null : this.parameters.get(index);
    }

    /**
     * Sets a new list of parameters to this operator.
     *
     * @param parameters The list of parameters to set.
     * @throws NullPointerException if the specified parameters is null.
     */
    public final void setParameters(final List<TypedSymbol> parameters) {
        if (parameters == null) {
            throw new NullPointerException();
        }
        this.parameters = parameters;
    }

    /**
     * Returns the goal description that represents the preconditions of the operator.
     *
     * @return The goal description that represents the preconditions of the operator.
     */
    public final Exp getPreconditions() {
        return this.preconditions;
    }

    /**
     * Sets new preconditions to the operator.
     *
     * @param preconditions The new goal description that represents the preconditions of the
     *                      operator to set.
     * @throws NullPointerException if the specified preconditions is null.
     */
    public final void setPreconditions(final Exp preconditions) {
        if (preconditions == null) {
            throw new NullPointerException();
        }
        this.preconditions = preconditions;
    }

    /**
     * Returns the goal description that represents the effects of the operator.
     *
     * @return The goal description that represents the effects of the operator.
     */
    public final Exp getEffects() {
        return this.effects;
    }

    /**
     * Sets new effects to the operator.
     *
     * @param effects he new goal description that represents the effects of the operator to set.
     * @throws NullPointerException if the specified effects is null.
     */
    public final void setEffects(final Exp effects) {
        if (effects == null) {
            throw new NullPointerException();
        }
        this.effects = effects;
    }

    /**
     * Returns the goal description that represents the duration constraints of the operator.
     *
     * @return the goal description that represents the duration constraints of the operator.
     */
    public final Exp getDuration() {
        return this.duration;
    }

    /**
     * Sets new duration constraints to the operator.
     *
     * @param duration the duration constraint to set
     */
    public final void setDuration(final Exp duration) {
        this.duration = duration;
    }

    /**
     * Normalizes the operators.
     *
     * @see Exp#renameVariables()
     * @see Exp#moveNegationInward()
     */
    public void normalize() throws FatalException {
        this.normalize(0);
    }

    /**
     * Normalizes the operators.
     *
     * @param index the index of the first variable, index.e., ?Xi.
     * @see Exp#renameVariables()
     * @see Exp#moveNegationInward()
     */
    public void normalize(int index) throws FatalException {
        int i = index;
        // Rename the parameters
        final Map<String, String> context = new LinkedHashMap<>();
        final List<TypedSymbol> parameters = this.getParameters();
        for (final TypedSymbol params : parameters) {
            final String image = params.renameVariables(i);
            context.put(image, params.getImage());
            i++;
        }
        // A hack to remove single atom in precondition
        if (this.preconditions.isLiteral()) {
            Exp atom = this.preconditions;
            this.preconditions = new Exp(Connective.AND);
            this.preconditions.addChild(atom);
        }
        // Rename the preconditions
        this.getPreconditions().renameVariables(context);
        this.getPreconditions().moveNegationInward();
        // Rename the effects
        // A hack to remove single atom in precondition
        if (this.effects.isLiteral()) {
            Exp atom = this.effects;
            this.effects = new Exp(Connective.AND);
            this.effects.addChild(atom);
        }
        this.getEffects().renameVariables(context);
        this.getEffects().moveNegationInward();
        // Rename the duration if the operator is a durative action.
        if (this.getDuration() != null) {
            this.getDuration().renameVariables(context);
        }
    }

    /**
     * Return the arity of the operator, i.e., the number of parameters of the operator.
     *
     * @return the arity of the operator.
     */
    public final int getArity() {
        return this.parameters.size();
    }

    /**
     * Return if this operator is equals to another object.
     *
     * @param object the other object.
     * @return <code>true</code> if <code>object</code> is not <code>null</code>, is an instance of
     *          the class <code>Op</code>, and has the same name; otherwise it returns <code>false</code>.
     * @see Object#equals(Object)
     */
    @Override
    public boolean equals(final Object object) {
        if (object != null && object instanceof Op) {
            final Op other = (Op) object;
            return this.name.equals(other.name);
        }
        return false;
    }

    /**
     * Returns the hash code value of the operator.
     *
     * @return the hash code value of the operator.
     * @see Object#hashCode()
     */
    @Override
    public int hashCode() {
        return this.name.hashCode();
    }

    /**
     * Returns a PDDL string representation of the operator.
     *
     * @return a string PDDL representation of the operator.
     */
    @Override
    public String toString() {
        final StringBuilder str = new StringBuilder();
        if (this.duration == null) {
            str.append("(:action ");
        } else {
            str.append("(:durative-action ");
        }
        str.append(this.name.toString()).append("\n").append(":parameters (");
        for (int i = 0; i < this.parameters.size() - 1; i++) {
            str.append(this.parameters.get(i)).append(" ");
        }
        if (!this.parameters.isEmpty()) {
            str.append(this.parameters.get(this.parameters.size() - 1).toString());
        }
        str.append(")");
        if (this.duration != null) {
            str.append("\n:duration ").append("\n  ").append(this.duration.toString()).append("\n:condition ");
        } else {
            str.append("\n:precondition ");
        }
        str.append("\n  ").append(this.preconditions.toString()).append("\n:effect ").append("\n  ")
            .append(this.effects.toString()).append("\n)");
        return str.toString();
    }

    public String print () {
        String res = "";
        res += "  (:durative-action " + name + "\n" +
                "   :parameters (" + sparametersToString() + ")\n" +
                "   :duration (= ?duration 10 ) \n" +                           //deal with duration
                "   :condition (and \n";
        res += printAtStartPositive(pos_pred_atstart);
        res += printOverall(pos_pred_overall);
        res += printAtEndPositive(pos_pred_atend);
        res += "   )\n   :effect (and \n";
        res += printAtStartPositive(pos_eff_atstart);
        res += printAtStartNegative(neg_eff_atstart);
        res += printAtEndPositive(pos_eff_atend);
        res += printAtEndNegative(neg_eff_atend);
        res += "   )\n" +
                ")";
        return res;
    }

    public String printActionWithGeneralAux (Set<Pair<Exp,String>> gen_pos_aux, Set<Pair<Exp,String>> gen_neg_aux) {
        String res = "";
        res += "  (:durative-action " + name + "\n" +
                "   :parameters (" + sparametersToString() + ")\n" +
                "   :duration (= ?duration 10 ) \n" +                           //deal with duration
                "   :condition (and \n";
        res += printAtStartPositive(pos_pred_atstart);
        res += printGeneralAuxStart(gen_pos_aux,gen_neg_aux);
        res += printOverall(pos_pred_overall);
        res += printAtEndPositive(pos_pred_atend);
        res += printGeneralAuxEnd(gen_pos_aux,gen_neg_aux);
        res += "   )\n   :effect (and \n";
        res += printAtStartPositive(pos_eff_atstart);
        res += printAtStartNegative(neg_eff_atstart);
        res += printAtEndPositive(pos_eff_atend);
        res += printAtEndNegative(neg_eff_atend);
        res += "   )\n" +
                ")";
        return res;
    }

    private String printGeneralAuxStart(Set<Pair<Exp, String>> gen_pos_aux, Set<Pair<Exp, String>> gen_neg_aux) {
        String res = "";
        for (Exp e : pos_eff_atstart) {
            for (Pair<Exp, String> p : gen_pos_aux) {
                if (p.first.getAtom().get(0).equals(e.getAtom().get(0))  )
                    res += "   (at start (" + "posaux" + "_" + e.toString().replace("(","").replace(")","") + "))\n";
            }
        }
        for (Exp e : neg_eff_atstart) {
            for (Pair<Exp, String> p : gen_neg_aux) {
                if (p.first.getAtom().get(0).equals(e.getAtom().get(0))  )
                    res += "   (at start (" + "negaux" + "_" + e.toString().replace("(","").replace(")","") + "))\n";
            }
        }
        return res;
    }


    private String printGeneralAuxEnd( Set<Pair<Exp, String>> gen_pos_aux, Set<Pair<Exp, String>> gen_neg_aux) {
        String res = "";
        for (Exp e : pos_eff_atend) {
            for (Pair<Exp, String> p : gen_pos_aux) {
                if (p.first.getAtom().get(0).equals(e.getAtom().get(0)) )
                    res += "   (at end (" + "posaux" + "_" + e.toString().replace("(","").replace(")","") + "))\n";
            }
        }
        for (Exp e : neg_eff_atend) {
            for (Pair<Exp, String> p : gen_neg_aux) {
                if (p.first.getAtom().get(0).equals(e.getAtom().get(0)) )
                    res += "   (at end (" + "negaux" + "_" + e.toString().replace("(","").replace(")","") + "))\n";
            }
        }
        return res;
    }

    public String sparametersToString () {
        String res = "";
        for (Pair<String,String> p : sparameters) {
            res += " ?" + p.first + " - " + p.second + " ";
        }
        return res;
    }

    private String printAtStartPositive(Set<Exp> pos_atstart) {
        String res = "";
        for (Exp e : pos_atstart)
            res += "   (at start " + e + ")\n";
        return res;
    }

    private String printAtEndPositive(Set<Exp> pos_atend) {
        String res = "";
        for (Exp e : pos_atend)
            res += "   (at end " + e + ")\n";
        return res;
    }

    private String printAtStartNegative(Set<Exp> neg_atstart) {
        String res = "";
        for (Exp e : neg_atstart)
            res += "   (at start (not " + e + "))\n";
        return res;
    }

    private String printAtEndNegative(Set<Exp> neg_atend) {
        String res = "";
        for (Exp e : neg_atend)
            res += "   (at end (not " + e + "))\n";
        return res;
    }

    private String printOverall(Set<Exp> overall) {
        String res = "";
        for (Exp e : overall)
            res += "   (over all " + e + ")\n";
        return res;
    }

}
