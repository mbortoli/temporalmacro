package com.grips.scheduler.asp.util;

import com.grips.persistence.domain.Atom;
import lombok.Getter;

@Getter
public class TempAtom {


    private boolean isPredicate;   //true -> predicate   //false -> function

    private String name;

    private String attributes;   //attribute names separated by a comma

    private double result; //result of a function



    public TempAtom(Atom at) {
        this.isPredicate = at.isPredicate();
        this.name = at.getName();
        this.attributes = at.getAttributes();
        this.result = at.getResult();
    }

    public TempAtom(String name, String attributes) {
        this.isPredicate = true;
        this.name = name;
        this.attributes = attributes;
    }

    public TempAtom(String name, String attributes, double result) {
        this.isPredicate = false;
        this.name = name;
        this.attributes = attributes;
        this.result = result;
    }

    public double getResult () {
        return this.result;
    }

    public String toString () {
        return (name + " " + attributes);
    }

    public String prettyPrinter () {
        String s = "";
        if (!isPredicate){
            s += "(= ";
            s += "(" + name;
        }
        else
            s += "(" + name;
        String [] attributesArray = attributes.split(";");
        for(int i = 0; i < attributesArray.length; i++)
            s += " " + attributesArray[i];
        s += ")";
        if (!isPredicate){
            s += " " + result + ")";
        }
        s += "\n";
        return s;
    }
}
