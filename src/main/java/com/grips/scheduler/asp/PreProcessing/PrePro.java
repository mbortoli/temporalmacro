package com.grips.scheduler.asp.PreProcessing;

import com.grips.scheduler.asp.parser.*;
import lombok.extern.apachecommons.CommonsLog;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@CommonsLog
public class PrePro {


    private static Set<Pair<Exp,String>> g_pos_aux;
    private static Set<Pair<Exp,String>> g_neg_aux;

    public static void main (String domainFile) {
        log.info("domain" + domainFile);

        g_pos_aux = new HashSet<>();
        g_neg_aux = new HashSet<>();

        Parser parser = new Parser();
        try {
            parser.parseDomain(domainFile);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }

        final Domain domain = parser.getDomain();


        List<Op> actions = domain.getOperators();
        for (Op op: actions) {
            op.createsparameter();
            op.generateLists();
        }



        //log.info(domain.toString());


        List<Pair<String,String>> param = new ArrayList<>();
        List<Pair<String,String>> paramAction = new ArrayList<>();
        param.add(new Pair<>("to","bs"));
        paramAction.add(new Pair<>("move","getBaseFromBScriticalTask".toLowerCase()));
        MacroOp op1 = mergeActions(domain,"move","getBaseFromBScriticalTask".toLowerCase(),param,paramAction);


        param = new ArrayList<>();
        paramAction = new ArrayList<>();
        param.add(new Pair<>("to","csi"));
        paramAction.add(new Pair<>("move","deliverProductC0ToCScriticalTask".toLowerCase()));
        MacroOp op2 = mergeActions(domain,"move","deliverProductC0ToCScriticalTask".toLowerCase(),param,paramAction);

        param = new ArrayList<>();
        paramAction = new ArrayList<>();
        param.add(new Pair<>("to","csi"));
        paramAction.add(new Pair<>("move","deliverProductC1ToCScriticalTask".toLowerCase()));
        MacroOp op3 = mergeActions(domain,"move","deliverProductC1ToCScriticalTask".toLowerCase(),param,paramAction);


        param = new ArrayList<>();
        paramAction = new ArrayList<>();
        param.add(new Pair<>("to","csi"));
        paramAction.add(new Pair<>("move","deliverProductC2ToCScriticalTask".toLowerCase()));
        MacroOp op4 = mergeActions(domain,"move","deliverProductC2ToCScriticalTask".toLowerCase(),param,paramAction);


        param = new ArrayList<>();
        paramAction = new ArrayList<>();
        param.add(new Pair<>("to","csi"));
        paramAction.add(new Pair<>("move","deliverProductC3ToCScriticalTask".toLowerCase()));
        MacroOp op5 = mergeActions(domain,"move","deliverProductC3ToCScriticalTask".toLowerCase(),param,paramAction);


        param = new ArrayList<>();
        paramAction = new ArrayList<>();
        param.add(new Pair<>("to","csi"));
        paramAction.add(new Pair<>("move","getCapBaseFromCSresourceTask".toLowerCase()));
        MacroOp op6 = mergeActions(domain,"move","getCapBaseFromCSresourceTask".toLowerCase(),param,paramAction);


        param = new ArrayList<>();
        paramAction = new ArrayList<>();
        param.add(new Pair<>("to","cso"));
        paramAction.add(new Pair<>("move","getProductFromCScriticalTask".toLowerCase()));
        MacroOp op7 = mergeActions(domain,"move","getProductFromCScriticalTask".toLowerCase(),param,paramAction);

        param = new ArrayList<>();
        paramAction = new ArrayList<>();
        param.add(new Pair<>("to","rsi"));
        paramAction.add(new Pair<>("move","deliverProductToRS1criticalTask".toLowerCase()));
        MacroOp op8 = mergeActions(domain,"move","deliverProductToRS1criticalTask".toLowerCase(),param,paramAction);

        param = new ArrayList<>();
        paramAction = new ArrayList<>();
        param.add(new Pair<>("to","rsi"));
        paramAction.add(new Pair<>("move","deliverProductToRS2criticalTask".toLowerCase()));
        MacroOp op9 = mergeActions(domain,"move","deliverProductToRS2criticalTask".toLowerCase(),param,paramAction);

        param = new ArrayList<>();
        paramAction = new ArrayList<>();
        param.add(new Pair<>("to","rsi"));
        paramAction.add(new Pair<>("move","deliverProductToRS3criticalTask".toLowerCase()));
        MacroOp op10 = mergeActions(domain,"move","deliverProductToRS3criticalTask".toLowerCase(),param,paramAction);

        param = new ArrayList<>();
        paramAction = new ArrayList<>();
        param.add(new Pair<>("to","rso"));
        paramAction.add(new Pair<>("move","getProductFromRSCriticalTask".toLowerCase()));
        MacroOp op11 = mergeActions(domain,"move","getProductFromRSCriticalTask".toLowerCase(),param,paramAction);

        param = new ArrayList<>();
        paramAction = new ArrayList<>();
        param.add(new Pair<>("to","bs"));
        paramAction.add(new Pair<>("move","getBaseFromBSResourceTask".toLowerCase()));
        MacroOp op12 = mergeActions(domain,"move","getBaseFromBSResourceTask".toLowerCase(),param,paramAction);

        param = new ArrayList<>();
        paramAction = new ArrayList<>();
        param.add(new Pair<>("to","cso"));
        paramAction.add(new Pair<>("move","getBaseFromCSResourceTask".toLowerCase()));
        MacroOp op13 = mergeActions(domain,"move","getBaseFromCSResourceTask".toLowerCase(),param,paramAction);


        param = new ArrayList<>();
        paramAction = new ArrayList<>();
        param.add(new Pair<>("to","rsi"));
        paramAction.add(new Pair<>("move","deliverBaseToRSResourceTask".toLowerCase()));
        MacroOp op14 = mergeActions(domain,"move","deliverBaseToRSResourceTask".toLowerCase(),param,paramAction);


        param = new ArrayList<>();
        paramAction = new ArrayList<>();
        param.add(new Pair<>("to","ds"));
        paramAction.add(new Pair<>("move","deliverProductToDScriticalTask".toLowerCase()));
        MacroOp op15 = mergeActions(domain,"move","deliverProductToDScriticalTask".toLowerCase(),param,paramAction);








        System.out.println(op1.printMacroWithGeneralAux(g_pos_aux,g_neg_aux));
        System.out.println(op2.printMacroWithGeneralAux(g_pos_aux,g_neg_aux));
        System.out.println(op3.printMacroWithGeneralAux(g_pos_aux,g_neg_aux));
        System.out.println(op4.printMacroWithGeneralAux(g_pos_aux,g_neg_aux));
        System.out.println(op5.printMacroWithGeneralAux(g_pos_aux,g_neg_aux));
        System.out.println(op6.printMacroWithGeneralAux(g_pos_aux,g_neg_aux));
        System.out.println(op7.printMacroWithGeneralAux(g_pos_aux,g_neg_aux));
        System.out.println(op8.printMacroWithGeneralAux(g_pos_aux,g_neg_aux));
        System.out.println(op9.printMacroWithGeneralAux(g_pos_aux,g_neg_aux));
        System.out.println(op10.printMacroWithGeneralAux(g_pos_aux,g_neg_aux));
        System.out.println(op11.printMacroWithGeneralAux(g_pos_aux,g_neg_aux));
        System.out.println(op12.printMacroWithGeneralAux(g_pos_aux,g_neg_aux));
        System.out.println(op13.printMacroWithGeneralAux(g_pos_aux,g_neg_aux));
        System.out.println(op14.printMacroWithGeneralAux(g_pos_aux,g_neg_aux));
        System.out.println(op15.printMacroWithGeneralAux(g_pos_aux,g_neg_aux));


        List<String> deletedActions = new ArrayList<>();
        deletedActions.add("move");
        deletedActions.add("getBaseFromBScriticalTask".toLowerCase());
        deletedActions.add("deliverProductC0ToCScriticalTask".toLowerCase());
        deletedActions.add("deliverProductC1ToCScriticalTask".toLowerCase());
        deletedActions.add("deliverProductC2ToCScriticalTask".toLowerCase());
        deletedActions.add("deliverProductC3ToCScriticalTask".toLowerCase());
        deletedActions.add("getCapBaseFromCSresourceTask".toLowerCase());
        deletedActions.add("getProductFromCScriticalTask".toLowerCase());
        deletedActions.add("deliverProductToRS1criticalTask".toLowerCase());
        deletedActions.add("deliverProductToRS2criticalTask".toLowerCase());
        deletedActions.add("deliverProductToRS3criticalTask".toLowerCase());
        deletedActions.add("getProductFromRSCriticalTask".toLowerCase());
        deletedActions.add("getBaseFromBSResourceTask".toLowerCase());
        deletedActions.add("getBaseFromCSResourceTask".toLowerCase());
        deletedActions.add("deliverBaseToRSResourceTask".toLowerCase());
        deletedActions.add("deliverProductToDScriticalTask".toLowerCase());


        for (Op op: actions) {
            if (!deletedActions.contains(op.getName().toString()))
                System.out.println(op.printActionWithGeneralAux(g_pos_aux,g_neg_aux));
        }

        genererateAuxForProblemFiles(parser);

        System.out.println(auxToStringWithType());



    }

    private static void genererateAuxForProblemFiles(Parser parser) {
        try {
            parser.parseProblem("./../../tproblem.pddl");
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
        final Problem problem = parser.getProblem();
        //System.out.println("pri " + problem.getObjects());

        final Domain domain = parser.getDomain();

        try {
            FileWriter myWriter = new FileWriter("./../../aux.txt");
            myWriter.write(auxToStringWithType());
            myWriter.close();
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

        TypedSymbol t = domain.getTypes().get(1);
        System.out.println("tpy " + domain.getTypes());
        //System.out.println("ttt " + t.getTypes());

        try {
            FileWriter myWriter = new FileWriter("./../../types.txt");
            myWriter.write(domain.getTypes().toString());
            myWriter.close();
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

    }

    private static String auxToStringWithType() {
        String res = "";
        Set<String> collect = new HashSet<>();
        for (Pair<Exp, String> p : g_pos_aux) {
            //res += "(" + p.second + ")\n";
            collect.add(p.second);
        }
        for (Pair<Exp, String> p : g_neg_aux) {
            //res += "(" + p.second + ")\n";
            collect.add(p.second);
            collect.add(negAuxToPosAux(p.second));
        }
        for (String s: collect)
            res += "(" + s + ")\n";
        return res;
    }

    private static String negAuxToPosAux (String aux) {
        //System.out.println("replacing " + aux + " with " + "pos" + aux.substring(3));
        return "pos" + aux.substring(3);
    }

    private static String auxToString() {
        String res = "";
        for (Pair<Exp, String> p : g_pos_aux) {
            res += "(" + p.second + ")\n";
        }
        for (Pair<Exp, String> p : g_neg_aux) {
            res += "(" + p.second + ")\n";
        }
        return res;
    }

    public static MacroOp mergeActions (Domain domain, String action1, String action2, List<Pair<String, String>> param, List<Pair<String, String>> paramAction) {

        List<Op> actions = domain.getOperators();

        Op op1 = null,op2 = null, realOp1 = null, realOp2 = null;
        for (Op op: actions) {
            //System.out.println("name " + op.getName().toString());
            if (op.getName().toString().equals(action1))
                realOp1 = op;
            else if (op.getName().toString().equals(action2))
                realOp2 = op;
        }

        op1 = new Op(realOp1);
        op2 = new Op(realOp2);

        op1.createsparameter();
        op2.createsparameter();

        op1.generateLists();


        op2.generateLists();


        System.out.println("action 1 " + op1.print());
        System.out.println("action 1 tosyting" + op1);





        //attributes merging
        int index = 0;
        for (Pair<String, String> pair : param) {
            mergeAttributes(pair,paramAction.get(index),op1,op2);
            index++;
        }



        System.out.println("action 1 " + op1.print());
        System.out.println("action 1 tosyting" + op1);

        System.out.println("action 2 " + op2.print());



        //preconditions at start
        Set<Exp> mid_neg_eff = setUnion(op1.getNeg_eff_atend(),op2.getNeg_eff_atstart());


        Set<Exp> mid_pos_eff = setUnion(op1.getPos_eff_atend(),op2.getPos_eff_atstart());

        Set<Exp> mid_neg_eff_MINUS_e1_pos_start = setMinus(mid_neg_eff,op1.getPos_eff_atstart());



        Set<Exp> p1_overall_UNION_p1_end = setUnion(op1.getPos_pred_atend(),op1.getPos_pred_overall());


        Set<Exp> p1_overall_UNION_p1_end_INTERSECTION_mid_neg_eff_MINUS_e1_pos_start = setIntersection(p1_overall_UNION_p1_end,mid_neg_eff_MINUS_e1_pos_start);


        Set<Exp> e1_pos_start_UNION_e1_pos_end = setUnion(op1.getPos_eff_atstart(),op1.getPos_eff_atend());
        Set<Exp> e2_neg_start_MINUS_e1_pos_start_UNION_e1_pos_end = setMinus(op2.getNeg_eff_atstart(),e1_pos_start_UNION_e1_pos_end);

        Set<Exp> p2_start_INTERSECTION_e2_neg_start_MINUS_e1_pos_start_UNION_e1_pos_end = setIntersection(op2.getPos_pred_atstart(),e2_neg_start_MINUS_e1_pos_start_UNION_e1_pos_end);

        //we start to build the precondition at start
        Set<Exp> p_start = setUnion(op1.getPos_pred_atstart(),p1_overall_UNION_p1_end_INTERSECTION_mid_neg_eff_MINUS_e1_pos_start);
        p_start = setUnion(p_start,p2_start_INTERSECTION_e2_neg_start_MINUS_e1_pos_start_UNION_e1_pos_end);
        //now we only need to add the aux predicates
        System.out.println(p_start);


        //+++++++++++++++++++overall

        //a)
        Set<Exp> mid_neg_eff_MINUS_e1_start_neg = setMinus(mid_neg_eff, op1.getNeg_eff_atstart());
        Set<Exp> p1_overall_UNION_p1_end_MINUS_mid_neg_eff_MINUS_e1_start_neg = setMinus(p1_overall_UNION_p1_end,mid_neg_eff_MINUS_e1_start_neg);

        //b)
        Set<Exp> e1_neg_start_UNION_e1_neg_end = setUnion(op1.getNeg_eff_atstart(),op1.getNeg_eff_atend());
        Set<Exp> e2_neg_start_MINUS_e1_neg_start_UNION_e1_neg_end = setMinus(op2.getNeg_eff_atstart(),e1_neg_start_UNION_e1_neg_end);
        Set<Exp> e1_pos_end_UNION_e2_neg_start_MINUS_e1_neg_start_UNION_e1_neg_end = setUnion(op1.getPos_eff_atend(),e2_neg_start_MINUS_e1_neg_start_UNION_e1_neg_end);
        Set<Exp> p2_start_MINUS_e1_pos_end_UNION_e2_neg_start_MINUS_e1_neg_start_UNION_e1_neg_end = setMinus(op2.getPos_pred_atstart(),e1_pos_end_UNION_e2_neg_start_MINUS_e1_neg_start_UNION_e1_neg_end);

        //c)
        Set<Exp> p2_overall_MINUS_mid_pos_eff = setMinus(op2.getPos_pred_overall(),mid_pos_eff);

        //result)
        Set<Exp> p_overall = setUnion(p1_overall_UNION_p1_end_MINUS_mid_neg_eff_MINUS_e1_start_neg,p2_start_MINUS_e1_pos_end_UNION_e2_neg_start_MINUS_e1_neg_start_UNION_e1_neg_end);
        p_overall = setUnion(p_overall,p2_overall_MINUS_mid_pos_eff);

        //+++++++++++++++++++++++++preconditions at end

        //result)
        Set<Exp> p_end = setMinus(op2.getPos_pred_atend(),mid_pos_eff);

        //++++++++++++++++++++++effects at start

        //a)
        Set<Exp> e_pos_start = setMinus(op1.getPos_eff_atstart(),mid_neg_eff);
        Set<Exp> e_neg_start = setMinus(op1.getNeg_eff_atstart(),mid_neg_eff);

        //b)
        e_neg_start = setUnion(e_neg_start,mid_neg_eff);

        //++++++++++++++++++++++effects at end

        //a)
        Set<Exp> e_pos_end = op2.getPos_eff_atend();
        Set<Exp> e_neg_end = op2.getNeg_eff_atend();

        //b)
        Set<Exp> e2_neg_start_UNION_e2_neg_end = setUnion(op2.getNeg_eff_atstart(),op2.getNeg_eff_atend());
        Set<Exp> mid_pos_eff_MINUS_e2_neg_start_UNION_e2_neg_end = setMinus(mid_pos_eff,e2_neg_start_UNION_e2_neg_end);

        e_pos_end = setUnion(e_pos_end,mid_pos_eff_MINUS_e2_neg_start_UNION_e2_neg_end);


        //AUX PREDICATES
        //positive (can not be made positive)
        Set<Pair<Exp,String>> pos_aux = new HashSet<>();
        for (Exp e: mid_neg_eff) {
            pos_aux.add(new Pair<>(e,"posaux" + "_" + e.toString().replace("(","").replace(")","")));
        }
        //System.out.println("posaux " + pos_aux);
        //negative (can not be made negative)
        Set<Pair<Exp,String>> neg_aux = new HashSet<>();
        for (Exp e: mid_pos_eff_MINUS_e2_neg_start_UNION_e2_neg_end) {
            neg_aux.add(new Pair<>(e,"negaux" + "_" + e.toString().replace("(","").replace(")","")));
        }
        Set<Exp> p1_overall_UNION_p1_end_INTERSECTION_mid_neg_eff = setIntersection(p1_overall_UNION_p1_end,mid_neg_eff);
        for (Exp e: p1_overall_UNION_p1_end_INTERSECTION_mid_neg_eff) {             //check here why we removed p1_pos_start
            neg_aux.add(new Pair<>(e,"negaux" + "_" + e.toString().replace("(","").replace(")","")));
        }
        Set<Exp> p2_start_INTERSECTION_e2_neg_start_MINUS_e1_pos_end = setIntersection(op2.getPos_pred_atstart(),setMinus(op2.getNeg_eff_atstart(),op1.getPos_eff_atend()));
        for (Exp e: p2_start_INTERSECTION_e2_neg_start_MINUS_e1_pos_end) {             //check here why we removed e1_pos_start
            neg_aux.add(new Pair<>(e,"negaux" + "_" + e.toString().replace("(","").replace(")","")));
        }
        Set<Exp> p2_overall_UNION_p2_end_INTERSECTION_mid_pos_eff = setIntersection(setUnion(op2.getPos_pred_overall(),op2.getPos_pred_atend()),mid_pos_eff);
        for (Exp e: p2_overall_UNION_p2_end_INTERSECTION_mid_pos_eff) {
            neg_aux.add(new Pair<>(e,"negaux" + "_" + e.toString().replace("(","").replace(")","")));
        }
        //System.out.println("negaux " + neg_aux);



        //DEFINING THE PARAMETERS
        MacroOp macro = new MacroOp(op1.getName().toString() + "_" + op2.getName().toString(), p_start, p_end, p_overall, e_neg_start, e_pos_start, e_neg_end, e_pos_end);
        macro.defineParameters(op1.getSparameters(),op2.getSparameters());
        macro.setPos_aux(pos_aux);
        macro.setNeg_aux(neg_aux);


        addAuxWithoutDuplicate(g_pos_aux,pos_aux);
        addAuxWithoutDuplicate(g_neg_aux,neg_aux);

        //g_pos_aux.addAll(pos_aux);
        //g_neg_aux.addAll(neg_aux);



        //System.out.println(macro.toString());
        return macro;

    }

    private static void addAuxWithoutDuplicate(Set<Pair<Exp, String>> g_aux, Set<Pair<Exp, String>> aux) {
        for (Pair<Exp, String> a: aux) {
            boolean found = false;
            for (Pair<Exp, String> g: g_aux) {
                if (g.second.substring(0,g.second.indexOf(" ")).equals(a.second.substring(0,a.second.indexOf(" ")))) {
                    found = true;
                }
            }
            if (!found) {
                g_aux.add(a);
            }
        }
    }

    private static void mergeAttributes(Pair<String, String> pair, Pair<String, String> actionName, Op op1, Op op2) {
        if (op1.getName().toString().equals(actionName.first) && op2.getName().toString().equals(actionName.second) ) {
            String toType = op2.getParamType(pair.second);
            op1.replaceParameter(pair.first,pair.second,toType);
        } else if (op1.getName().toString().equals(actionName.second) && op2.getName().toString().equals(actionName.first) ) {
            String toType = op1.getParamType(pair.first);
            op2.replaceParameter(pair.second,pair.first,toType);
        } else {
            System.out.println(op1.getName() + " " + actionName.first + " " + op2.getName() + " " + actionName.second);
            log.error ("Error in merging parameters, case non handled");
        }





    }


    public static Set<Exp> setUnion (Set<Exp> set1, Set<Exp> set2) {
        Set<Exp> result = new HashSet<Exp>();
        result.addAll(set1);
        result.addAll(set2);
        return result;
    }

    public static Set<Exp> setMinus (Set<Exp> set1, Set<Exp> set2) {
        Set<Exp> result = new HashSet<Exp>();
        result.addAll(set1);
        result.removeAll(set2);
        return result;
    }

    public static Set<Exp> setIntersection (Set<Exp> set1, Set<Exp> set2) {
        Set<Exp> result = new HashSet<Exp>();
        result.addAll(set1);
        result.retainAll(set2);
        return result;
    }



}


