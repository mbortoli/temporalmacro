package com.grips.scheduler.asp.PreProcessing;

import com.grips.scheduler.asp.parser.Exp;
import com.grips.scheduler.asp.parser.Symbol;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class MacroOp {

    private String name;
    private Set<Exp> pos_pred_atstart;

    private Set<Exp> pos_pred_atend;

    private Set<Exp> pos_pred_overall;

    private Set<Exp> neg_eff_atstart;
    private Set<Exp> pos_eff_atstart;

    private Set<Exp> neg_eff_atend;
    private Set<Exp> pos_eff_atend;

    private List<Pair<String,String>> sparameters;

    private Set<Pair<Exp,String>> pos_aux;
    private Set<Pair<Exp,String>> neg_aux;

    public MacroOp(String name, Set<Exp> pos_pred_atstart, Set<Exp> pos_pred_atend, Set<Exp> pos_pred_overall, Set<Exp> neg_eff_atstart, Set<Exp> pos_eff_atstart, Set<Exp> neg_eff_atend, Set<Exp> pos_eff_atend) {
        this.name = name;
        this.pos_pred_atstart = pos_pred_atstart;
        this.pos_pred_atend = pos_pred_atend;
        this.pos_pred_overall = pos_pred_overall;
        this.neg_eff_atstart = neg_eff_atstart;
        this.pos_eff_atstart = pos_eff_atstart;
        this.neg_eff_atend = neg_eff_atend;
        this.pos_eff_atend = pos_eff_atend;
        sparameters = new ArrayList<>();
    }

    public String sparametersToString () {
        String res = "";
        for (Pair<String,String> p : sparameters) {
            res += " ?" + p.first + " - " + p.second + " ";
        }
        return res;
    }

    public Set<Pair<Exp, String>> getPos_aux() {
        return pos_aux;
    }

    public void setPos_aux(Set<Pair<Exp, String>> pos_aux) {
        this.pos_aux = pos_aux;
    }

    public Set<Pair<Exp, String>> getNeg_aux() {
        return neg_aux;
    }

    public String printMacro () {
        String res = "";
        res += "  (:durative-action " + name + "\n" +
                "   :parameters (" + sparametersToString() + ")\n" +
                "   :duration (= ?duration 10 ) \n" +                           //deal with duration
                "   :condition (and \n";
        res += printAtStartPositive(pos_pred_atstart);
        res += printOverall(pos_pred_overall);
        res += printAtEndPositive(pos_pred_atend);
        res += "   )\n   :effect (and \n";
        res += printAtStartPositive(pos_eff_atstart);
        res += printAtStartNegative(neg_eff_atstart);
        res += printAtEndPositive(pos_eff_atend);
        res += printAtEndNegative(neg_eff_atend);
        res += "   )\n" +
                ")";
        return res;
    }

    public String printMacroWithAux () {
        String res = "";
        res += "  (:durative-action " + name + "\n" +
                "   :parameters (" + sparametersToString() + ")\n" +
                "   :duration (= ?duration 10 ) \n" +                           //deal with duration
                "   :condition (and \n";
        res += printAtStartPositive(pos_pred_atstart);
        res += printOverall(pos_pred_overall);
        res += printAtEndPositive(pos_pred_atend);
        res += printAuxAtStart();
        res += "   )\n   :effect (and \n";
        res += printAtStartPositive(pos_eff_atstart);
        res += printAtStartNegative(neg_eff_atstart);
        res += printAuxAtStartEff();
        res += printAtEndPositive(pos_eff_atend);
        res += printAtEndNegative(neg_eff_atend);
        res += printAuxAtEndEff();
        res += "   )\n" +
                ")";
        return res;
    }

    public String printMacroWithGeneralAux (Set<Pair<Exp,String>> gen_pos_aux, Set<Pair<Exp,String>> gen_neg_aux) {
        String res = "";
        res += "  (:durative-action " + name + "\n" +
                "   :parameters (" + sparametersToString() + ")\n" +
                "   :duration (= ?duration 10 ) \n" +                           //deal with duration
                "   :condition (and \n";
        res += printAtStartPositive(pos_pred_atstart);
        res += printGeneralAuxStart(gen_pos_aux,gen_neg_aux);
        res += printOverall(pos_pred_overall);
        res += printAtEndPositive(pos_pred_atend);
        res += printGeneralAuxEnd(gen_pos_aux,gen_neg_aux);
        res += printAuxAtStart();
        res += printOppositeNegAux();
        res += "   )\n   :effect (and \n";
        res += printAtStartPositive(pos_eff_atstart);
        res += printAtStartNegative(neg_eff_atstart);
        res += printAuxAtStartEff();
        res += printAtEndPositive(pos_eff_atend);
        res += printAtEndNegative(neg_eff_atend);
        res += printAuxAtEndEff();
        res += "   )\n" +
                ")";
        return res;
    }

    private String printOppositeNegAux() {
        String res = "";
        for (Pair<Exp, String> p: neg_aux ) {
            if (!containsPredicate(p.first.getAtom().get(0),pos_aux)) {
                res += "   (at start (" + negAuxToPosAux(p.second) + "))\n";
            }
        }
        return res;
    }

    private String negAuxToPosAux (String aux) {
        System.out.println("replacing " + aux + " with " + "pos" + aux.substring(3));
        return "pos" + aux.substring(3);
    }

    private String printGeneralAuxStart(Set<Pair<Exp, String>> gen_pos_aux, Set<Pair<Exp, String>> gen_neg_aux) {
        String res = "";
        for (Exp e : pos_eff_atstart) {
            for (Pair<Exp, String> p : gen_pos_aux) {
                if (p.first.getAtom().get(0).equals(e.getAtom().get(0)) && !containsPredicate(e.getAtom().get(0),pos_aux) )
                    res += "   (at start (" + "posaux" + "_" + e.toString().replace("(","").replace(")","") + "))\n";
            }
        }
        for (Exp e : neg_eff_atstart) {
            for (Pair<Exp, String> p : gen_neg_aux) {
                if (p.first.getAtom().get(0).equals(e.getAtom().get(0)) && !containsPredicate(e.getAtom().get(0),neg_aux) )
                    res += "   (at start (" + "negaux" + "_" + e.toString().replace("(","").replace(")","") + "))\n";
            }
        }
        return res;
    }

    private boolean containsPredicate (Symbol predicate,Set<Pair<Exp, String>> aux) {
        for (Pair<Exp, String> p : aux) {
            if (p.first.getAtom().get(0).equals(predicate))
                return true;
        }
        return false;
    }

    private String printGeneralAuxEnd( Set<Pair<Exp, String>> gen_pos_aux, Set<Pair<Exp, String>> gen_neg_aux) {
        String res = "";
        for (Exp e : pos_eff_atend) {
            for (Pair<Exp, String> p : gen_pos_aux) {
                if (p.first.getAtom().get(0).equals(e.getAtom().get(0)) && !containsPredicate(e.getAtom().get(0),pos_aux) )
                    res += "   (at end (" + "posaux" + "_" + e.toString().replace("(","").replace(")","") + "))\n";
            }
        }
        for (Exp e : neg_eff_atend) {
            for (Pair<Exp, String> p : gen_neg_aux) {
                if (p.first.getAtom().get(0).equals(e.getAtom().get(0)) && !containsPredicate(e.getAtom().get(0),neg_aux) )
                    res += "   (at end (" + "negaux" + "_" + e.toString().replace("(","").replace(")","") + "))\n";
            }
        }
        return res;
    }

    private String printAuxAtStart() {
        String res = "";
        for (Pair<Exp, String> p : pos_aux) {
            res += "   (at start (" + p.second + "))\n";
        }
        for (Pair<Exp, String> p : neg_aux) {
            res += "   (at start (" + p.second + "))\n";
        }
        return res;
    }

    private String printAuxAtStartEff() {
        String res = "";
        for (Pair<Exp, String> p : pos_aux) {
            res += "   (at start (not (" + p.second + ")))\n";
        }
        for (Pair<Exp, String> p : neg_aux) {
            res += "   (at start (not (" + p.second + ")))\n";
        }
        return res;
    }

    private String printAuxAtEndEff() {
        String res = "";
        for (Pair<Exp, String> p : pos_aux) {
            res += "   (at end (" + p.second + "))\n";
        }
        for (Pair<Exp, String> p : neg_aux) {
            res += "   (at end (" + p.second + "))\n";
        }
        return res;
    }

    private String printAtStartPositive(Set<Exp> pos_atstart) {
        String res = "";
        for (Exp e : pos_atstart)
            res += "   (at start " + e + ")\n";
        return res;
    }

    private String printAtEndPositive(Set<Exp> pos_atend) {
        String res = "";
        for (Exp e : pos_atend)
            res += "   (at end " + e + ")\n";
        return res;
    }

    private String printAtStartNegative(Set<Exp> neg_atstart) {
        String res = "";
        for (Exp e : neg_atstart)
            res += "   (at start (not " + e + "))\n";
        return res;
    }

    private String printAtEndNegative(Set<Exp> neg_atend) {
        String res = "";
        for (Exp e : neg_atend)
            res += "   (at end (not " + e + "))\n";
        return res;
    }

    private String printOverall(Set<Exp> overall) {
        String res = "";
        for (Exp e : overall)
            res += "   (over all " + e + ")\n";
        return res;
    }

    public void setNeg_aux(Set<Pair<Exp, String>> neg_aux) {
        this.neg_aux = neg_aux;
    }

    public String getName() {
        return name;
    }

    public Set<Exp> getPos_pred_atstart() {
        return pos_pred_atstart;
    }

    public Set<Exp> getPos_pred_atend() {
        return pos_pred_atend;
    }

    public Set<Exp> getPos_pred_overall() {
        return pos_pred_overall;
    }

    public Set<Exp> getNeg_eff_atstart() {
        return neg_eff_atstart;
    }

    public Set<Exp> getPos_eff_atstart() {
        return pos_eff_atstart;
    }

    public Set<Exp> getNeg_eff_atend() {
        return neg_eff_atend;
    }

    public Set<Exp> getPos_eff_atend() {
        return pos_eff_atend;
    }

    @Override
    public String toString() {
        return "MacroOp{" +
                "name='" + name + '\'' +
                ", pos_pred_atstart=" + pos_pred_atstart +
                ", pos_pred_atend=" + pos_pred_atend +
                ", pos_pred_overall=" + pos_pred_overall +
                ", neg_eff_atstart=" + neg_eff_atstart +
                ", pos_eff_atstart=" + pos_eff_atstart +
                ", neg_eff_atend=" + neg_eff_atend +
                ", pos_eff_atend=" + pos_eff_atend +
                ", sparameters=" + sparameters +
                ", pos_aux=" + pos_aux +
                ", neg_aux=" + neg_aux +
                '}';
    }

    public void defineParameters(List<Pair<String, String>> sparameters1, List<Pair<String, String>> sparameters2) {
        for (Pair<String, String> p1 : sparameters1) {
                sparameters.add(p1);
        }
        for (Pair<String, String> p2 : sparameters2) {
            if (!sparameters1.contains(p2))
                sparameters.add(p2);
        }
    }
}